// Написать скрипт: при нажатии на кнопку, добавлять в div с идентификатором
// «container» фрагмент разметки:
// <div class="item">
//   <h3>Заголовок</h3>
//   <p>текст текст текст</p>
// </div>;

/**
 * Задание.
 *
 * Создать элемент h1 с текстом «Нажмите любую клавишу.».
 *
 * При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
 * «Нажатая клавиша: ИМЯ_КЛАВИШИ».
 
// const elemente = document.createElement('h1');
// elemente.innerHTML = 'Нажмите любую клавишу.'
// document.body.append(elemente)
// window.addEventListener('keydown', (event)=> {
//     elemente.innerHTML = `Нажатая клавиша: ${event.key}`
// })

let h1 = document.createElement("h1");
h1.innerText = "Нажмите любую клавишу.";
document.body.append(h1);
document.body.addEventListener("keydown", (event) => {
    h1.innerText = `Нажатая клавиша: ${event.code}`;
});
*/
/** */
const h1 = document.createElement("h1");
h1.innerText = "Нажмите любую клавишу";
document.body.append(h1);
document.addEventListener("keyup", (event) => {
    h1.innerText = `Нажатая клавиша: ${event.code}`; //  покажет клавиши
    // h1.innerText = `Нажатая клавиша: ${event.key}`; // event.key покажет букву алфавита
});

/**
 * При натисканні shift та "+"" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 

document.addEventListener("keyup", (event) => {
    if (event.key === "+" && event.shiftKey) {
        fontSize++;
        document.body.style.fontSize = `${fontSize}px`;
    }
    if ((event.key === "_" || event.key === "-") && event.shiftKey) {
        fontSize--;
        document.body.style.fontSize = `${fontSize}px`;
    }
    if (event.key === "0" && event.ctrlKey) {
        //если кнтрл + 0, то обнуляем стиль
        fontSize = 16;
        document.body.style.fontSize = `${fontSize}px`;
    }
});
window.addEventListener("keyup", function(event) {
    if (event.key === "+" && event.shiftKey) {
        if (fontSize < 30) {
            fontSize++;
            document.body.style.fontSize = `${fontSize}px`;
        }
        if (event.key === "-" && event.shiftKey) {}
    }
});*/