// Добавить в Html кнопку ScrollToTop.
// По дефолту кнопка не видна.
// Когда пользователь проскроллит больше чем высота окна (window.innerHeight) - показать кнопку.
// Кнопка должна быть фиксированная внизу странички.
// По клику на нее - нужно проскролить в начало документа.
// (https://developer.mozilla.org/ru/docs/Web/API/Window/scrollTo)

const scrollButton = document.querySelector(".scroll-to-top-btn")

window.addEventListener("scroll", e => {
    if (window.pageYOffset > window.innerHeight) {
        scrollButton.classList.add("active")
    } else {
        scrollButton.classList.remove("active")
    }
} )
scrollButton.addEventListener("click", () => {
    window.scrollTo({top:0, left:window.pageXOffset, behavior:"smooth"})
})