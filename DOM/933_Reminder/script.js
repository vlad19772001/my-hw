/**
 * Задание 1. * Написать программу для напоминаний.
 * Все модальные окна реализовать через alert.
 * Условия:
 * - Если пользователь не ввёл сообщение для напоминания — вывести alert с
 * сообщением «Ведите текст напоминания.»;
 * - Если пользователь не ввёл значение секунд,через сколько нужно вывести напоминание —
 *   вывести alert с сообщением «Время задержки должно быть больше одной секунды.»;
 * - Если все данные введены верно, при клике по кнопке «Напомнить» необходимо
 * её блокировать так,
 *   чтобы повторный клик стал возможен после полного завершения текущего напоминания;
 * - После этого вернуть изначальные значения обоих полей;
 * - Создавать напоминание, если внутри одного из двух элементов input нажать
 * клавишу Enter;
 * - После загрузки страницы установить фокус в текстовый input.
 */
const inputReminder = document.querySelector("#reminder");
const inputSeconds = document.querySelector("#seconds");
const button = document.querySelector("button");
const handler = () => {
    if (inputReminder.value === "") {
        alert("Ведите текст напоминания");
        return null;
    }
    if (!inputSeconds.value || +inputSeconds.value < 1) {
        alert("Время задержки должно быть больше одной секунды.");
        return null;
    }
    button.setAttribute("disabled", "");
    setTimeout(() => {
        alert(`${inputReminder.value}`);
        button.removeAttribute("disabled");
        inputSeconds.value = 0;
        inputReminder.value = "";
    }, inputSeconds.value * 1000);
};
button.addEventListener("click", handler);