/**
 * Написать программу,
 * которая будет спрашивать у пользователя ссылку на кратинку,
 * потом отображать эту картинку на экране мигающим образом -
 * скрывать и показывать каждую секунду
 *
 *

const  imageLink = prompt("enter the link to image")
const image = document.createElement('img')
image.setAttribute('src', imageLink)
document.body.append(image)
let blink = () => {
    image.style.display = 'none'
    setTimeout(() => image.style.display = 'block', 1000)
}
setInterval(blink, 2000)
*/

// D:\DAN IT\my-hw\DOM\971\IMG\53.png

const imageLink = prompt("enter link");
const image = document.createElement("img");
image.setAttribute("src", imageLink);
document.body.append(image);

setInterval(() => {
    image.style.display = "none";
    setTimeout(() => (image.style.display = "block"), 1000);
}, 2000);
/*
for (let i = 1; i <= 10; i++) {
    setTimeout(() => {
        console.log(i);
    }, 1000 * i);
}*/