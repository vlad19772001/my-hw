/**
 * Задание 1.
 *
 * Написать скрипт, который создаст квадрат произвольного размера.
 *
 * Размер квадрата в пикселях получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в некорректном формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Все стили для квадрата задать через JavaScript посредством одной строки кода.
 *
 * Тип элемента, описывающего квадрат — div.
 * Задать ново-созданному элементу CSS-класс .square.
 *
 * Квадрат в виде стилизированного элемента div необходимо
 * сделать первым и единственным потомком body документа.
 

*/
let sizeSide = +prompt("enter size");
const newElement = document.createElement("div");
newElement.innerText = " my -SQUARE- square";
newElement.className = "square";
newElement.style.width = `${sizeSide}px`;
newElement.style.height = `${sizeSide}px`;
document.body.prepend(newElement);

/*
*
    
    *
    
    */

// let square = null;
// do {
//     square = +prompt("Enter param for square")
// } while (!square || !Number.isInteger(square) || Number.isNaN(square));
// // указываем размеры квадрата в пикселях и цвет
// const style = `width: ${square}px; height: ${square}px; background: red;`;
// let div = document.createElement("div")
// div.style = style;
// div.classList.add("square")
// document.body.prepend(div); // append - в конец среди потомков