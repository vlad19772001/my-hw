/**
 * Улучшить существующий код:
 * - Доавить каждому элементу класс "item" и id, в котором будет указан его порядковый номер;
 * - Добавить кнопку удаления к каждому элементу списка. задать кнопке текст: "Х". Присвоить ей класс 'deleteButton';
 * - По клику на кнопку соответствующий элемент должен удаляться;
 * - Использовать делегирование событий;
 *
 */
let input = document.querySelector("input");
let list = document.querySelector(".tasks-list");
let button = document.querySelector("#clear");

function createToDoItem(event) {
    const inputValue = input.value.trim();
    if (event.key === "Enter" && inputValue.length) {
        let listElement = document.createElement("li");
        listElement.classList.add("item");
        listElement.setAttribute("id", `${list.childElementCount}+1`);

        const button2 = document.createElement("button");
        button2.innerText = "X";
        button2.classList.add("deleteButton");

        listElement.innerText = inputValue;
        input.value = "";
        list.append(listElement);
        listElement.append(button2);

        /* // слушатель на каждую кнопку Х
                                                    button2.addEventListener("click", () => {
                                                        listElement.remove();
                                                    }); */
    }
}

const clearAll = () => {
    const answer = confirm("Are you sure?");
    if (answer) {
        list.innerHTML = "";
    }
};

const onKeyDown = (event) => {
    if (list.childElementCount > 0 && event.code === "KeyD" && event.ctrlKey) {
        event.preventDefault();
        list.lastChild.remove();
    }
    if (event.key === "Backspace" && event.altKey && event.shiftKey) {
        clearAll();
    }
};

button.addEventListener("click", clearAll);
input.addEventListener("keydown", createToDoItem);
document.addEventListener("keydown", onKeyDown);
/* вешаем слушателя на весь список айтемов, и если у кнопки класс-удалить, 
то его родителя удаляем */
list.addEventListener("click", (event) => {
    if (
        event.target.tagName === "BUTTON" &&
        event.target.classList.contains("deleteButton")
    ) {
        event.target.parentNode.remove();
    }
});