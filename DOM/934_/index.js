// реализовать баннер по использование cookies, который будет фиксированным внизу страницы
// и занимать треть экрана.
// В нем должен быть текст и кнопка. По клику на эту кнопку - баннер должен скрываться
// и после перезагрузки страницы не показываться вновь
const  btn = document.querySelector('#buttonCookies')
const  text = document.querySelector('#cookiesBanner')
btn.addEventListener('click', () => {
    text.style.display = 'none';
    localStorage.setItem('cookies', 'accept')
})
window.addEventListener('load', () => {
    if (localStorage.getItem('cookies')) {
        text.style.display = 'none';

    }
})