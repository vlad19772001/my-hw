/**
 * Задание 2.
 *
 * На экране указан список товаров с указанием названия и количества на складе.
 *
 * Найти товары, которые закончились и:
 * - Изменить 0 на «закончился»;
 * - Изменить цвет текста на красный;
 * - Изменить жирность текста на 600.
 *
 * Требования:
 * - Цвет элемента изменить посредством модификации атрибута style.
 */
const store = document.getElementsByTagName("li");
// console.log(store);
for (const item of store) {
    if (item.innerText.includes(": 0")) {
        item.innerText = item.innerText.replace(": 0", ": закончился");
        item.style.color = "red";
        item.style.fontWeight = "700";
    }
}

// const store = document.getElementsByTagName('li')
// let string = ": 0";
// for (item of store) {
//     if (item.innerText.includes(string)) {
//         item.innerText = item.innerText.replace(string, ": закончился")
//         item.style.color = 'red';
//         item.style.fontWeight = 900;

//         // item.style = "color:green; font-weight:600;"

//     }

// }

// console.log(store);

// const elements = document.getElementsByTagName("li");
// for ( let item of elements) {
//     let string = ": 0"
//     if (item.innerText.includes(string)) { // ищем кусок текста, и заменяем его
//         item.innerText = item.innerText.replace(string, ": закончился")
//         item.style.color = "red" // и меняем ему свойства
//         item.style.fontWeight = "800";
//         item.style = "color:green; font-weight:600;"
//     }
//     console.log(item.innerText);
// }