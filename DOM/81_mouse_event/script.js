// Задание. создаст баттон с текстом войти
// при клике выводить алерт "добро пожаловать"

// const button = document.createElement('button')
// button.innerText = 'click me'
// button.addEventListener('click', () => {
//     alert('добро пожаловать')
// } )
// document.body.append(button)

/* Улучшить скрипт из предыдущего задания.
 *
 * При наведении на кнопку  указателем мыши, выводить alert с сообщением:
 * «При клике по кнопке вы войдёте в систему.».
 *
 * Сообщение должно выводиться один раз.
 *
 * Условия:
 * - Решить задачу грамотно.
 */

// const button = document.createElement('button')
// button.innerText = 'click me'
// button.addEventListener('click', () => {
//     alert('добро пожаловать')
// } )

// button.addEventListener('mouseover', () => {
//     console.log('При клике по кнопке вы войдёте в систему.')
// }, {once:true} )

// document.body.append(button)

// pause 0-45   dom nodens 22/11

// ​const button = document.createElement("button")
// button.innerText = "click me"
// button.addEventListener("click", ()=> {
//     alert("dkalksdlaskdn")
// })
// button.addEventListener("mouseover", ()=> {
//     alert("ри клике по кнопке вы войдёте в систему.")
// }, {once:true})
// document.body.append(button)

// Задание 2.

// Задание. создать кнопку баттон с текстом "ВОЙТИ"
// при клике выводить алерт "добро пожаловать"

const button = document.createElement("button");
button.innerText = "ВОЙТИ";
button.addEventListener("click", () => {
    alert("Добро пожаловать");
});
document.body.append(button);

/* Улучшить скрипт из предыдущего задания.
 *
 * При наведении на кнопку  указателем мыши, выводить alert с сообщением:
 * «При клике по кнопке вы войдёте в систему.».
 *
 * Сообщение должно выводиться один раз.
 *
 * Условия:
 * - Решить задачу грамотно.
 */
button.addEventListener(
    "mouseover",
    () => {
        alert("При клике по кнопке вы войдёте в систему.");
    }, { once: true }
);