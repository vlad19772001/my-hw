// 'use strict'
/**
 * Задание 1.
 *
 * При загрузке страницы посмотреть - есть ли в localStorage значение по ключу userName?
 * Если есть - выводить сообщение "Hello, userName" на экран,
 * где вместо userName должно быть вставлено значение по одноименному ключу в localStorage
 * Если значения по такому ключу нет - спросить у пользователя имя, модальным окном, записать его в localStorage.
 * После этого вывести сообщение "Hello, userName" на экран,
 * где вместо userName должно быть вставлено значение по одноименному ключу в localStorage

window.addEventListener("load", (event) => {
    let userName = localStorage.getItem("userName");
    if (!userName) {
        userName = prompt("Write your name");
        localStorage.setItem("userName", userName);
        alert(`Hello, ${userName}`);
    } else {
        alert(`Hello, ${userName}`);
    }
});
 */
// window.addEventListener('load', event => {
//     let userName = localStorage.getItem('userName');
//     if (!userName) {
//         userName = prompt("enter name")
//         localStorage.setItem('userName', userName)
//     }         alert(`hello, ${userName}`)
// } )

/**
 * Задание 2.
 *
 * Сделать форму логина с сохранением данных в localStorage.
 
input 2шт по айди, кнопка будет добавлять, append (добавить на страницу)
потом найти их, считать и сохранить в локал стораж, отдельно нейм и значение 
слушатель на кнопке

const userName = document.createElement("input");
const userPasw = document.createElement("input");
const button = document.createElement("button");
button.innerText = "save";
document.body.append(userName, userPasw, button);

button.addEventListener("click", () => {
    localStorage.setItem("name", userName.value);
    localStorage.setItem("pswrd", userPasw.value);
});*/

// const userName = document.createElement('input');
// const userPass = document.createElement("input");
// const btn = document.createElement("button");
// btn.innerText = "Submit"
// btn.addEventListener('click', () => {
//     localStorage.setItem.length('name', userName)
//     localStorage.setItem.length('pswrd', userPass)
// })

/**
 * Реализовать форму ввода данных для пользователя
 * ФИО,
 * номер телефона,
 * почта,
 * возраст
 *
 * Данные сохранять в localStorage в виде объекта,
 * и при загрузке страницы - считывать их и заполнять форму этими данными
 * При сохранении имитировать отправку данных на сервер,
 * блокируя кнопку сохранения на 2 секунды.
 * */
/**/
const name = document.createElement("input");
const phone = document.createElement("input");
const email = document.createElement("input");
const age = document.createElement("input");
const btn = document.createElement("button");
btn.innerText = "Submit";

btn.setAttribute("type", "submit");
name.setAttribute("type", "name");
name.setAttribute("placeholder", "Enter name");

phone.setAttribute("type", "phone");
phone.setAttribute("placeholder", "Enter phone");

email.setAttribute("type", "email");
email.setAttribute("placeholder", "Enter email");

age.setAttribute("type", "number");
age.setAttribute("placeholder", "Enter age");

document.body.append(name, phone, email, age, btn);

const handler = () => {
    btn.setAttribute("disabled", "");
    const user = {
        name: name.value,
        phone: phone.value,
        email: email.value,
        age: age.value,
    };
    localStorage.setItem("user", JSON.stringify(user));
    setTimeout(() => {
        btn.removeAttribute("disabled");
    }, 2000);
};
btn.addEventListener("click", handler);

window.addEventListener("load", (event) => {
    let user2 = localStorage.getItem("user");
    const parsed = JSON.parse(user2);
    name.value = parsed.name;
    phone.value = parsed.phone;
    email.value = parsed.email;
    age.value = parsed.age;
});

// function handler() {
//     btn.setAttribute("disabled", "");
//     const user = {
//         name: name.value,
//         phone: phone.value,
//         email: email.value,
//         age: age.value,
//     };
//     localStorage.setItem("user", JSON.stringify(user));
//     setTimeout(function() {
//         btn.removeAttribute("disabled");
//         2000;
//     });
// }
// window.addEventListener("load", (event) => {
//     let user2 = localStorage.getItem("user");
//     const parsed = JSON.parse(user2);
//     // name.value = parsed.name;
//     // phone.value = parsed.phone;
//     // email.value = parsed.email;
//     // age.value = parsed.age;
// });
// btn.addEventListener("click", handler);