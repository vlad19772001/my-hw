/**
 * Задание 3.
 *
 * Создать элемент h1 с текстом «Добро пожаловать!».
 *
 * Под элементом h1 добавить элемент button c текстом «Раскрасить».
 *
 * При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.
 */
/* Дано */
const PHRASE = "Добро пожаловать!";

function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return `rgb(${r}, ${g}, ${b})`;
}
//
/* Решение */
/*
let hone = document.createElement('h1')
let button = document.createElement('button')
hone.innerText = PHRASE
button.innerText = 'Раскрасить'
document.body.append(hone)
document.body.append(button)
// button.addEventListener('click', ()=> {
//   hone.innerText = ''
// for (i=0; i<PHRASE.length; i++) {
//   let letter = document.createElement('a')
//   letter.style.color = getRandomColor()
//   letter.innerText = PHRASE[i];
//   hone.append(letter)
// }
// })  второй вариант вешаем слушателя на бади и на движение мышки:
document.body.addEventListener ('mousemove', ()=> {
  hone.innerText = ''
for (i=0; i<PHRASE.length; i++) {
  let letter = document.createElement('a')
  letter.style.color = getRandomColor()
  letter.innerText = PHRASE[i];
  hone.append(letter)
}
})*/

/*
// на каждую букву в классе:
// const h1 = document.createElement('h1')
// const btn = document.createElement('button')
// h1.innerText = PHRASE
// document.body.append(h1)
// btn.innerText = 'Раскрасить!!'
// document.body.append(btn)

// btn.addEventListener('click', ()=>{
//   h1.innerText = '';
//   for (let i = 0; i < PHRASE.length; i++){
//     let span = document.createElement('span')
//     span.innerText = PHRASE[i]
//     span.style.color = getRandomColor()
//     h1.append(span);
//   }

//   for (let char of PHRASE){
//     let span = document.createElement('span')
//     span.innerText = char
//     span.style.color = getRandomColor()
//     h1.append(span);
//   }
// })

// // при наведение цвета меняются
// const handler = () => {
//   h1.innerText = '';
//   for (let i = 0; i < PHRASE.length; i++){
//     let span = document.createElement('span')
//     span.innerText = PHRASE[i]
//     span.style.color = getRandomColor()
//     h1.append(span);
//   }
//   // for (let char of PHRASE){
//   //   let span = document.createElement('span')
//   //   span.innerText = char
//   //   span.style.color = getRandomColor()
//   //   h1.append(span);
//   // }
// }

// btn.addEventListener('click', handler)
// document.body.addEventListener('mousemove', handler)
*/

// my NEW variant
element = document.createElement("h1");
element.innerText = PHRASE;
document.body.append(element);

button = document.createElement("button");
button.innerText = "Раскрасить";
document.body.append(button);
const handler = function() {
    element.innerText = "";
    for (let char of PHRASE) {
        let newLetter = document.createElement("span");
        newLetter.innerText = char;
        newLetter.style.color = getRandomColor();
        element.append(newLetter);
    }
};
button.addEventListener("click", handler);
document.body.addEventListener("mousemove", handler); // cлушатель на движение мишки