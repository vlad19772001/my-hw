/**
 * При натисканні на кнопку Validate відображати
 * VALID зеленим кольром, якщо значення проходить валідацію
 * INVALID червоним кольором, якщо значення не проходить валідацію
 *
 * Правила валідації значення:
 * - значення не пусте
 *
 * ADVANCED
 * Правила валідації значення:
 * - повинно містити щонайменше 5 символів
 * - не повинно містити пробілів
 * - повинно починатися з літери (потрібно використати регулярні вирази)
 *
 */
// let validate = document.getElementById("validate-btn");

// validate.addEventListener('click', () => {
//     let input = document.getElementById('input').value;
//     // let input2 = input.value;
//     let div = document.createElement('div');
//     // console.log(input);

// if (input !== '' && input.length > 5 && !input.includes(' ') && input[0].match(/^[a-z]/) ) {
//     div.innerText  = 'VALID'
//     div.style.color = 'green';
// } else {
//     div.innerText  = 'INVALID'
//     div.style.color = 'red';
// }
// validate.after(div);
// })

// в ИФ проверяем чтобы инпут был не пустой, был  длинее 5ти символов, не включал пробел, и первая симвов был буквой (а не числом)
/* у меня дивы добавляет постоянно к старым, а в классе делает каждый раз новый
т.к. через функцию сделано */

// const buttonClick = document.getElementById("validate-btn")
// let valid = document.createElement("div")
// const handler = function () {
// const inputField = document.getElementById("input")
//     let content = inputField.value

//     if (content && content.length > 5) {
//             valid.innerText = "VALID"
//         valid.style.color = "green"
// } else {
//             valid.innerText = "INVALID"
//         valid.style.color = "red"
// }
//     document.body.append(valid)
// }
// buttonClick.addEventListener("click", handler)

const button = document.getElementById("validate-btn");
let valid = document.createElement("div");

const handler = function() {
    const inputField = document.getElementById("input");
    let content = inputField.value;

    if (content) {
        valid.innerText = "VALID";
        valid.style.color = "green";
        valid.style.fontSize = "44px";
    } else {
        valid.innerText = "INVALID";
        valid.style.color = "red";
    }
    document.body.append(valid);
};
button.addEventListener("click", handler);