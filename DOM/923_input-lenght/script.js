// Даны инпуты. Сделайте так, чтобы все инпуты по потери фокуса проверяли свое содержимое
// на правильное количество символов. Сколько символов должно быть в инпуте,
// указывается в атрибуте data-length.
// Если вбито правильное количество, то граница инпута становится зеленой,
// если неправильное - красной.

const inputs = document.querySelectorAll("input");
// const inputs = document.getElementsByTagName("input"); don`t work ForEach
inputs.forEach((input) => {
    input.addEventListener("blur", (event) => {
        let maxLenght = event.target.dataset.length;
        // console.log(maxLenght);
        if (event.target.value.length > maxLenght) {
            event.target.style.border = "10px solid red";
        } else {
            event.target.style.border = "10px green solid ";
        }
    });
});