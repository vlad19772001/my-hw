/**
 * Задание 3.
 *
 * Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
 *
 * Цвет тёмных ячеек — #161619.
 * Цвет светлых ячеек — #FFFFFF.
 * Остальные стили CSS для доски и ячеек готовы.
 *
 * Доску необходимо развернуть внутри элемента с классом .board.
 *
 * Каждая ячейка доски представляет элемент div с классом .cell.
 */

/* Дано */
const LIGHT_CELL = '#ffffff';
const DARK_CELL = '#161619';
const V_CELLS = 8;
const H_CELLS = 8;

const board = document.createElement('section');

board.classList.add('board');

document.body.prepend(board);



















/* Решение */

const fillChessBoard = () => {
        let row = 0;
        for (let i = 0; i < 64; i++) {
            const whiteSquare = `background-color: ${LIGHT_CELL}; width: ${V_CELLS}; height: ${H_CELLS};`;
            const blackSquare = `background-color: ${DARK_CELL}; width: ${V_CELLS}; height: ${H_CELLS};`;
            const cell = document.createElement("div")
            cell.classList.add('cell')
            if (i % 8 === 0) {
                row++;
            }
            const rowIsEven = row % 2 === 0;
            const cellIsEven = i % 2 === 0;

            if (rowIsEven && cellIsEven) {
                cell.style = whiteSquare;
            } else if (rowIsEven && !cellIsEven) {
                cell.style = blackSquare;
            } else if (!rowIsEven && cellIsEven {
                    cell.style = blackSquare;
                } else if (!rowIsEven && !cellIsEven) {
                    cell.style = whiteSquare;
                }
                board.append(cell)
            }
        }
        fillChessBoard()