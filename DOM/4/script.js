/**
 * Задание 4.
 *
 * Написать программа для редактирования количества остатка продуктов на складе магазина.
 *
 * Программа должна запрашивать название товара для редактирования.
 * Если ввёденного товара на складе нет — программа проводит повторный запрос названия товара
 * до тех пор, пока соответствующее название не будет введено.
 *
 * После чего программа запрашивает новое количество товара.
 * После чего программа вносит изменения на веб-страницу: заменяет остаток указанного товара его новым количеством.
 */
let productName = null;
let newQuantity = null;
const goodsList = document.querySelectorAll("li");
const allList = document.querySelector("ul");

while (!productName) {
    productName = prompt("Enter towar");
    goodsList.forEach((item) => {
        if (item.innerText.toLowerCase().includes(productName.toLowerCase())) {
            newQuantity = +prompt("Ввведи новое количество");
            const textElements = item.innerText.split(": "); // сплитом режем айтем на масив с двух частей
            item.innerText = `${textElements[0]} : ${newQuantity}`; // теперь
            // текст каждого айтема будет = нулевой елемент массива (кофе): + новове число
        }
    });
}

// let productName = null;
// let newQuantity = null;
// const goodsList = document.querySelectorAll('li');
// const allList = document.querySelector('ul');

// const changeGoodQuantity = () => {
//     goodsList.forEach(item => {
//         if (item.innerText.toLowerCase().includes(productName.toLowerCase())) {
//             while (!newQuantity || Number.isNaN(newQuantity)) {
//                 newQuantity = +prompt('Введите новое к-во товара');
//             }
//             const textElements = item.innerText.split(': ');
//             item.innerText = `${textElements[0]}: ${newQuantity}`
//         }
//     })
// }

// const checkIfGoodPresent = string => {
//         if (productName && allList.textContent.toLowerCase().includes(productName.toLowerCase())) {
//             changeGoodQuantity();
//         } else {
//             productName = prompt('Введите название товара')
//             checkIfGoodPresent(productName);
//         }
// }

// while (!productName) {
//     productName = prompt('Введите название товара')
//     checkIfGoodPresent(productName);
// }