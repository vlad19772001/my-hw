// получить элемент 
// const list = document.getElementById('list')
// const list2 = document.querySelector('#list')
// console.log('list', list);
// console.log('list2', list2);

// console.log(list.innerText); // выводить только текст
// console.log(list.innerHTML); // выводить  текст + html code
// console.log(list.outerHTML); // выводить  текст + html code + родитель

// по class name
// const listClass = document.getElementsByClassName("list-item")
// console.log("list-item", listClass);
// console.log(listClass[0].innerHTML);
// console.log(listClass[0].innerText);
// console.log(listClass[0].outerHTML);

// const listClas = document.querySelectorAll('.list-item')
// console.log(listClas);

// по тегу li
// const li = document.querySelectorAll('li')
// const li2 = document.getElementsByTagName('li')
// console.log(li, li2);

// третий ли по CSS селектору 
// const thirdLi = document.querySelector('li:nth-child(3)')
// console.log(thirdLi);

// все ли по CSS селектору li
// const allLi = document.querySelectorAll('li')
// console.log(allLi);

