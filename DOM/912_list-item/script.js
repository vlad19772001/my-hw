let input = document.getElementById("new-task");
let list = document.querySelector(".tasks-list");
let clearAll = document.getElementById("clear");
const addTask = function(event) {
    if (event.key === "Enter" && input.value.length) {
        let listElement = document.createElement("li");
        listElement.innerText = input.value;
        list.prepend(listElement);
        input.value = "";
    }
};
const delTask = function(event) {
    if (list.childElementCount > 0 && event.code === "KeyD" && event.ctrlKey) {
        event.preventDefault();
        const lastLi = list.querySelector("li:first-child");
        if (lastLi) {
            lastLi.remove();
        }
    }
    if (event.key === "Backspace" && event.altKey && event.shiftKey) {
        delAll();
    }
};
const delAll = function() {
    if (confirm("Are you sure?")) {
        list.innerHTML = "";
    }
};
input.addEventListener("keyup", addTask);
document.addEventListener("keyup", delTask);
clearAll.addEventListener("click", delAll);

/**
         * При натисканні на enter в полі вводу
         * додавати його значення, якщо воно не пусте,
         * до списку задач та очищувати поле вводу
         * При натисканні Ctrl + D на сторінці видаляти
         * останню додану задачу
         * Додати можливість очищувати весь список
         * Запустити очищення можна двома способами:
         * - при кліці на кнопку Clear all
         * - при натисканні на Alt + Shift + Backspace
         * При очищенні необхідно запитувати у користувача підтвердження
         * (показувати модальне вікно з вибором Ok / Cancel)
         * Якщо користувач підвердить видалення, то очищувати список,
         * інакше нічого не робити зі списком
         
        let input = document.getElementById('new-task')
        let list = document.querySelector('.tasks-list')
        const createToDoItem = event => {
            input.addEventListener('keydown', event => {
                const inputValue = input.value.trim() // trim  проверяет и обрезает пробел по краям, его не сохраняет в Ли
                if (event.key==='Enter'&& input.value.length) { 
                    const value = input.value;
                    let listElement = document.createElement('li')
                    listElement.innerHTML = value;
                    list.append(listElement)
                    input.value = ""
                }
            })

        }

        input.addEventListener('keydown', createToDoItem)
        */

// window.addEventListener('keyup',event => {
//     if (event.code === 'KeyD' && event.ctrlKey) {

//     }
// } )

// let input = document.getElementsByTagName("input")
// let input = document.querySelector("input") // ищем по селектору инпут
// let list = document.querySelector(".tasks-list")
// input.addEventListener("keydown", event => {
//     const inputValue = input.value.trim();
//     if (event.key === "Enter" && input.value.length ) {
//         const value = input.value;
//         let listElement = document.createElement("li");
//         listElement.innerText = input.value;
//         input.value = ""
//         list.append(listElement)

//     }
// })

// через функцию
// let input = document.querySelector("input") // ищем по селектору инпут
// let list = document.querySelector(".tasks-list")
// const createToDoList = event => {
// input.addEventListener("keydown", event => {
//     const inputValue = input.value.trim();
//     if (event.key === "Enter" && input.value.length ) {
//         // const value = input.value;
//         let listElement = document.createElement("li");
//         listElement.innerText = input.value;
//         input.value = ""
//         list.append(listElement)
//     }
// })}
//     function deleteLastItem(event) {
//         if (list.childElementCount > 0 && event.code === "KeyD" && event.ctrlKey) {
//             event.preventDefault();
//             list.lastChild.remove;
//             // const lastLi = list.querySelector("li:last-child")
//             // if (lastLi) {lastLi.remove}
//         }
//     }

// input.addEventListener("keydown", createToDoList);
// document.addEventListener("keydown", deleteLastItem);

// let input = document.querySelector('input');
// let list = document.querySelector('.tasks-list');
// let button = document.querySelector('#clear');
// function createToDoItem(event) {
//     const inputValue = input.value.trim();

//     if (event.key === 'Enter' && inputValue.length) {
//         let listElement = document.createElement('li');
//         listElement.innerText = inputValue;
//         input.value = '';
//         list.append(listElement);
//     }
// }

// const clearAll = () => {
//    const answer = confirm("Are you sure?")
//     if (answer){
//         list.innerHTML = "";
//     }
// }

// const onKeyDown = event => {
//     if (list.childElementCount > 0 && event.code === 'KeyD' && event.ctrlKey) {
//         event.preventDefault()
//         list.lastChild.remove()

//     }
//     if(event.key === "Backspace" && event.altKey && event.shiftKey){
//         clearAll();
//     }
// }

// button.addEventListener('click', clearAll)
// input.addEventListener('keydown', createToDoItem);
// document.addEventListener('keydown', onKeyDown);