// Дана таблица с юзерами с двумя колонками: имя и фамилия.
// Под таблицей сделайте форму, с помощью которой можно будет добавить нового юзера в таблицу.
// Сделайте так, чтобы при клике на любую ячейку появлялся prompt, с помощью которого можно изменить
// текст ячейки. Задачу решите с помощью делегирования (то есть событие должно быть навешано на table).

let name = null;
let surname = null;
const table = document.querySelector("table");
const form = document.createElement("form");
const inputName = document.createElement("input");
const inputSurname = document.createElement("input");
const saveButton = document.createElement("button");
saveButton.innerText = "SAVE";
saveButton.addEventListener("click", (event) => {
    event.preventDefault();
    name = inputName.value;
    surname = inputSurname.value;
    const row = document.createElement("tr");
    const nameCell = document.createElement("td");
    const surnameCell = document.createElement("td");
    nameCell.innerText = name;
    nameCell.classList.add("cell");
    surnameCell.classList.add("cell");
    row.append(nameCell);

    surnameCell.innerText = surname;
    row.append(surnameCell);

    table.append(row);
    inputName.value = "";
    inputSurname.value = "";
});
form.append(inputName, inputSurname, saveButton);
document.body.append(form);
const editPrompt = (event) => {
    if (
        event.target.classList.contains("cell") &&
        event.target.tagName !== "TH"
    ) {
        const editName = prompt("Enter new Names");
        const nameCell = document.createElement("td");
        event.target.innerText = editName;
    }
};
table.addEventListener("click", editPrompt);

/*
 *
 *
 *
 *
 */
/*
 *
 *
 *
 *
 */
/*
 *
 *
 *
 *
 */
/*
 *
 *
 *
 *
 */
/**let name = null;
        let surname = null;
        const form = document.createElement("form")
        const inputName = document.createElement("input")
        const inputSurname = document.createElement("input")
        const saveButton = document.createElement("button")
        const table = document.querySelector("table")


        inputName.setAttribute("id", "name")
        inputSurname.setAttribute("id", "surname")
        saveButton.innerText = "save"

        form.append(inputName, inputSurname, saveButton)
        document.body.append(form)

        saveButton.addEventListener("click", event => {
            event.preventDefault()
            const name = document.body.querySelector("#name").value
            const surname = document.body.querySelector("#surname").value
            const row = document.createElement("tr")
            const  nameCell = document.createElement("td")
            const  surnameCell = document.createElement("td")
            nameCell.innerText = name
            nameCell.classList.add("cell")

            row.append(nameCell)
            surnameCell.innerText = surname
            surnameCell.classList.add("cell")
            row.append(surnameCell)
            table.append(row)
        }) */