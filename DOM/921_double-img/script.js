// По клику на картинку - создавать дубль этой картинки, в конце списка картинок.
// Использовать делегирование событий

document.addEventListener("click", (event) => {
    let img = event.target;
    console.log(img.tagName);
    if (img.tagName === "IMG") {
        const copy = img.cloneNode();
        const images = document.querySelectorAll("img");
        images[images.length - 1].after(copy);
    }
});

// document.addEventListener("click", event => {
//     let targetImg = event.target
//     // console.log(targetImg.tagName);
//     if (targetImg.tagName === "IMG") {
//         let result = targetImg.cloneNode()
// document.body.append(result)
//     }
// }) то-же самое сокращаем:

// if (event.target.tagName === "IMG") {
//     const clone = event.target.cloneNode()
//     document.body.append(clone)
// }
// })