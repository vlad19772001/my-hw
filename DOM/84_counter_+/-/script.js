/**
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */
/*
let counter = document.getElementById('counter');
let decrement = document.getElementById('decrement-btn')
let increment = document.getElementById('increment-btn')
let conterNew = 0;
decrement.addEventListener('click', () => {
    conterNew--;
    counter.innerText = `Counter: ${conterNew}`;
})
increment.addEventListener('click', () => {
    conterNew++;
    counter.innerText = `Counter: ${conterNew}`;

})

// time 01:43 start

// const counter = document.getElementById("counter")
// const decrement = document.getElementById("decrement-btn") // -
// const increment = document.getElementById("increment-btn") // +
// let number = 0;
// decrement.addEventListener("click", ()=> {
//     if (number>0) {
//        number--;
//     counter.innerText = number 
//     }
    
// })
// increment.addEventListener("click", ()=> {
//     number++
//     counter.innerText = number
// })*/
let counter = document.getElementById("counter");
let decrement = document.getElementById("decrement-btn");
let increment = document.getElementById("increment-btn");
let count = 0;

decrement.addEventListener("click", () => {
    if (count > 0) {
        count--;
        counter.innerText = `Counter: ${count}`;
    }
});
increment.addEventListener("click", () => {
    count = count + 1;
    counter.innerText = `Counter: ${count}`;
});

counter.innerText = `Counter: ${count}`;