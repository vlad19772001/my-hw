/**
 * Задание 1.
 *
 * Получить элемент с классом .remove.
 * Удалить его из разметки.
 *
 * Получить элемент с классом .bigger.
 * Заменить ему CSS-класс .bigger на CSS-класс .active.
 *
 * Условия:
 * - Вторую часть задания решить в двух вариантах: в одну строку и в две строки.
 */

const elemRemove = document.getElementsByClassName("remove");
console.log(elemRemove);
elemRemove[0].remove();

const elemBigger = document.querySelector(".bigger");
console.log(elemBigger);
// elemBigger.className = "active";
elemBigger.classList.add("active");
elemBigger.classList.remove("bigger");

/*
/* Удаление */
// document.querySelector('.remove').remove();
// const listItem = document.querySelectorAll(".listItem");

/* Изменение: способ 1 */
// document.querySelector('.bigger').classList.replace('bigger', 'active');

/* Изменение: способ 2 */
// document.querySelector('.bigger').classList.remove('bigger');
// document.querySelector('.bigger').classList.add('active');

// const bigger = document.querySelector(".bigger")
// bigger.classList.remove("bigger");
// bigger.classList.add("sasss")

// document.querySelector('.remove').remove(); // удаляем сразу
// const bigger = document.querySelector('.bigger')
// // bigger.classList.replace('bigger','active') // заменяем сразу старый класс, на новый
// bigger.classList.remove('bigger'); // удаляем старый класс
// bigger.classList.add('active');// добавляем новый класс