/*Задание
Программа будет выводить то или иное сообщение на страницу в зависимости от возраста пользователя.
Сообщение представляет из себя html-элемент div. В нем будет сообщение (далее alert-message) и тип
(тип алерта - это тот или иной класс для div).
Логика:
Спросить у пользователя его имя и возраст.
Если число (также проверить, что там число) меньше 18, вывести на страницу
сообщение, где alert-message - Sorry, you are not be able to visit us. Тип - 'alert-danger'
Если больше 18 - alert-message 'Hi, ${name}.', тип 'alert-success';
*/
/*
let name = prompt("Enter your name, please");
let age = +prompt("Enter your age, please");
while (Number.isNaN(age) || !age) {
    age = +prompt("Enter your age, please");
}

let div = document.createElement("div");
let text = "";
if (age < 18) {
    text = "Sorry, you are not be able to visit us.";
    div.className = "alert-danger";
} else {
    text = `Hi, ${name}.`;
    div.className = "alert-success";
}

div.innerText = text;
document.body.prepend(div);
 */

// let age = +prompt("Enter your age");
// let name = prompt("Enter your name");
// let div = document.createElement("div")
// let alertMessage = '';
// document.body.prepend(div); // append - в конец среди потомков
// if (age<18) {
//     // alert("Sorry, you are not be able to visit us")
//     div.innerText = "Sorry, you are not be able to visit us";
//     div.classList.add('alert-danger')
// } else {
//     // alert(`Hi, ${name}.`)
//     div.innerText = `Hi, ${name}.`;
//     div.classList.add('alert-success')
// }

// в классе делали так:
// let age = null
// const name = prompt("Enter your name")
// do {
//     age = +prompt('Enter your age')
// } while (!age || Number.isNaN(age))

// let alert = document.createElement('div')

// if (age < 18) {
//     alert.classList.add('alert-danger')
//     alert.innerText = 'Sorry, you are not be able to visit us.'
// } else {
//     alert.classList.add('alert-success')
//     alert.innerText = `Hi, ${name}.`
// }

// document.body.prepend(alert)

// Написать ту ду лист.

// Спрашивать у пользователя пункты для добавления в список до тех пор, пока
// он не нажмет отмена. Каждый prompt это новый элемент списка.
// Все пункты вставить на страницу.
// Не забываем про семантику (список должен быть оформлен через ul или ol);
// По клику на элемент списка - удалить этот пункт.
// Подсказка
// // elem.onclick = function () {
//     пишем что происходит по клику
//   };
let itemList = [];

do {
    item = prompt("enter task");
    if (item) {
        itemList.push(item);
    }
} while (item !== null);

// let itemList = ["one", "two", "three", "fo", "five"];
const newUl = document.createElement("ul");
document.body.prepend(newUl);
itemList.forEach(function(item) {
    element = document.createElement("li");
    element.innerText = item;
    element.onclick = function() {
        element.remove();
    };
    newUl.append(element);
});

// -
// -
// -
// -
// -
// -

/*

            const todoList = [];
            let newItem ;
            while (newItem !== null) {
                newItem = prompt('enter to do item');
                todoList.push(newItem)
            }
            const orderedList = document.createElement('ol')
            todoList.filter(item=>item).forEach(item => {
                const listItem = document.createElement('li')
                listItem.innerText = item;
                listItem.onclick = function () {
                    listItem.remove()
                }
                orderedList.append(listItem)
            })

            document.body.append(orderedList);

            */

// в классе:
// const todoList = [];
// let newItem = '';

//  while (newItem !== null) {
//      newItem = prompt('Enter to do item');
//      todoList.push(newItem)
//  }

//  const orderedList = document.createElement('ol');

//  todoList
//      .filter(item => item)
//      .forEach(item => {
//          const listItem = document.createElement('li');
//          listItem.innerText = item;
//          listItem.onclick = function () {
//              listItem.remove()
//     }
//     orderedList.append(listItem);
// })

// document.body.append(orderedList); */