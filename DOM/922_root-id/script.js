// Напишите код, который при клике на любой div внутри root будет выводить в консоль его id.

const root = document.getElementById("root");
root.addEventListener("click", (event) => {
    console.log(event.target.id);
});