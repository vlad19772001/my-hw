"use strict";

/**
 * Задание 2.
 *
 * Написать функцию, capitalizeAndDoublify, которая переводит
 * символы строки в верхний регистр и дублирует каждый её символ.
 *
 * Условия:
 * - Использовать встроенную функцию repeat;
 * - Использовать встроенную функцию toUpperCase;
 * - Использовать цикл for...of.
 */

// let myText = "";
// let newText = "";
// function toDouble(myText) {
//     for (const char in myText) {
//         newText += myText[char].toUpperCase()+myText[char].toUpperCase();
//          }
//     }
// toDouble("vlad maks");
// console.log(newText);

// let toDouble2 = (myText) => {
//     let newText = "";
//     for (const char in myText) {
//         newText += myText[char].toUpperCase()+myText[char].toUpperCase();
//         }
// return console.log(newText);
// }
// toDouble2("maksym");

// =========== String & Number ==============
// let myText = "Kravchenko \nVladyslav"
// console.log(myText);
// console.log(myText.length +" char");
// console.log(myText[5]); // выведет 5-тую букву с нуля.
// for (let char of myText) {
// 	console.log(char); // выведет каждую букву с 0,1,2,3...
// }
// console.log(myText.toUpperCase()); // выведет в верхнем регистре.
// console.log(myText.slice(0, 8)); // выведет символы от 0 до 8 (не включ).
// console.log(myText.substr(11, 10)); // выведет 10-ть символов начин от 11.
// let myText = "135.65px"
// console.log(parseFloat(myText));//вернёт дробное число, без текста сзади
// console.log(parseInt(myText));//вернёт целое число, без текста сзади
// console.log(Math.max(5, 32, 3, -3, 40));
// console.log(Math.min(5, 32, 3, -3, 40));
// let numb = 58.858;
// console.log(numb);
// console.log(+numb.toFixed(2)); // toFixed выдаёт СТРОКУ
// console.log(Math.round(numb*10)/10);

// arrow
/**
 * Задача 1.
 *
 * Дан массив с днями недели.
 *
 * Вывести в консоль все дни недели с помощью:
 * - Классического цикла for;
 * - Цикла for...of;
 * - Специализированного метода для перебора элементов массива forEach.
 */

// /* Дано */
// const days = [
//     'Monday',
//     'Tuesday',
//     'Wednesday',
//     'Thursday',
//     'Friday',
//     'Saturday',
//     'Sunday',
// ];
// for (let i = 0; i<days.length; i++) {
//     console.log(days[i]);
// }
//
// for (let daysItem of days) {
//     console.log(daysItem);
//
// }
//
// days.forEach(function (item) {
//     console.log(`${item}`)
// })


/**
 * Задача 2.
 *
 * Написать мульти-язычную программу-органайзер.
 *
 * Программа спрашивает у пользователя на каком языке он желает увидеть список дней недели.
 * После ввода пользователем желаемого языка программа выводит в консоль дни недели на указанном языке.
 *
 * Доступные языки (локали): ua, ru, en.
 *
 * Если введённая пользователем локаль не совпадает с доступными — программа переспрашивает его до тех пор,
 * пока доступная локаль не будет введена.
 *
 * Условия:
 * - Решение должно быть коротким, лаконичным и универсальным;
 * - Всячески использовать методы массива;
 * - Использование метода Object.keys() обязательно.
 */

/* Дано */
// const days = {
//     ua: [
//         'Понеділок',
//         'Вівторок',
//         'Середа',
//         'Четвер',
//         'Пятниця',
//         'Субота',
//         'Неділя',
//     ],
//     ru: [
//         'Понедельник',
//         'Вторник',
//         'Среда',
//         'Четверг',
//         'Пятница',
//         'Суббота',
//         'Воскресенье',
//     ],
//     en: [
//         'Monday',
//         'Tuesday',
//         'Wednesday',
//         'Thursday',
//         'Friday',
//         'Saturday',
//         'Sunday',
//     ],
// };


// Создать массив чисел от 1 до 100.
// Отфильтровать его таким образом, чтобы в новый массив не попали числа меньше 10 и больше 50.
// Вывести в консоль новый массив.
// let number = [];
// for (let i = 0; i<100; i++) {
//     number.push(i)
// }
// let numberNew = number.filter(number=> number>10 && number<50 );
// console.log(numberNew)

// Есть массив брендов автомобилей ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам нужно получить новый массив, объектов типа
// {
//     type: 'car'
//     brand: ${элемент массива}
// }

// Вывести массив в консоль
// let car = ["bMw", "Audi", "teSLa", "toYOTA"];
// let carNew = car.map(item=> {
//     return {type:'car',
//         brand: item.toUpperCase(),}
// })
//
// console.log(carNew)

// let array = ["bMw", "Audi", "teSLa", "toYOTA"];

// let newArray = array.map(item => ({type: "car", brand: item.toUpperCase()}))
// console.log(newArray);
//
// const newArray1 = [];
//
// array.forEach(item => {
//     newArray1.push({
//         type: "car",
//         brand: item.toUpperCase(),
//     })
// })
//
// console.log(newArray1);

// Для всех машин добавить свойство isElectric, для теслы оно будет true, для остальных false;
// Для того чтоб проверять что машина электрическая создать отдельную функцию что будет хранить
// в себе массив брендов электрических машин и будет проверять входящий элемент - на то,
// если он в массиве.

// Создать новый массив, в котором хранятся все машины, что не являются электрокарами.
// Вывести его в модальное окно пользователю.

// const checkIsElectric = name => {
//     let electricCars = ["TESLA"]
//     return electricCars.includes(name.toUpperCase())
// }
//
// const createArray = array => {
//     const newArray = array.map(item=> ({type:"car",
//         brand: item.toUpperCase(),
//         isElectric: checkIsElectric(item) }))
//     return newArray.filter(car => car.isElectric===false)
// }
// console.log(createArray(array))



// Создать массив объектов students (в количестве 7 шт).
// У каждого студента должно быть имя, фамилия и направление обучения - Full-stask
// или Front-end. У каждого студента должен быть метод, sayHi, который возвращает строку
// `Привет, я ${имя}, студент Dan, направление ${направление}`.
// Перебрать каждый объект и вызвать у каждого объекта метод sayHi;

// const students = [
//     {   name: 'Ann',
//         lastName: 'Marela',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`)
//         }
//     },
//     {   name: 'John',
//         lastName: 'Lennon',
//         major: 'Full-stask',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`)
//         }
//         },
//
//     {   name: 'Eric',
//         lastName: 'Claptone',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`)
//         }
//         },
//
//     {   name: 'Riki',
//         lastName: 'Martin',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`) }
//         },
//
//     {   name: 'Elton',
//         lastName: 'John',
//         major: 'Full-stask',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`) }
//         },
//
// ]
// students.forEach(students=> {
//     students.sayHi();
// })


// students.forEach(student => {
//     student.sayHi = function () {
//         console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//     };
// })
//


/**
 *  Дан список игроков. Нужно отсортировать массив по убыванию, и добавить каждому игроку поле place,
 *  в котором будет указано
 *  его место в списке игроков - то есть Иван должен оказаться на первом месте, Василий - на последнем
 * */

const users = [
    { name: 'Victor', score: 20 },
    { name: 'Mario', score: 10 },
    { name: 'Tatiana', score: 30 },
    { name: 'Vasiliy', score: 0 },
    { name: 'Ivan', score: 100 },
];
const array = [
    {total: 1},
    {total: 2},
    {total: 3},
]
users.sort((a, b) => b.score - a.score);
const usersPlaces = users.map((item, index)=>) {
return {...item, place: index +1}
}







