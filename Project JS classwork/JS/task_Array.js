"use strict";

// -+-\_/-+-  -+-\_/-+-   ARRAY   -+-\_/-+-  -+-\_/-+- 
// const str = "Hello"
// const myArray = str.split() // преобразует текст в массив в одним словом
// const myArray2 = str.split("") // преобразует текст в массив с кучей букв
// const myArray3 = myArray2.reverse(); // записал элементы массива в обр порядке
// console.log(myArray3);
// const newStr = myArray2.join(";") // преобразует массив в строку с разделителем ;
// console.log(newStr);
// console.log(myArray)
// console.log(myArray2)
// console.log(myArray2.indexOf('H')) // ищет содержимое и возвращает его позицию(ключ)
// let myArray2new = myArray2.concat('добавил', 'четыре', 'слова' , 'word')
// console.log(myArray2new)
// console.log(myArray2new.slice(-3)) // вырезает(копирует) кусок массива, или весь если ()
// let poisk = myArray2new.find((znach, indeks, massiv)=>{
//     return znach !== 'четыре';
// })   сокращаем без {return}
// let poisk = myArray2new.find((znach, indeks, massiv)=> znach !== 'четыре')
// console.log(poisk)
// let poiskFiltr = myArray2new.filter((znach, indeks, massiv)=> znach !== 'четыре')
// console.log(poiskFiltr)

// const month = ['may', 'september']
// month.push('october')
// month.unshift('January', 'February')
// month[2] = 'March'
// console.log(month.splice(0, 1)) // вырезает из массива
// console.log(month)

// for (var i = 0; i < myArray.length; i++) {
//     console.log(myArray[i]);
//   }
//   console.log(emp[2]); // элемент № 3  (0,1,2)
//   console.log(myArray[2]); // элемент № 3 (0,1,2)
//   console.log(myArray); // все элементы  
// let myArray = [
//     {name: "Dima", age: 14 },
//     {name: "Olha", age: 18 },
//     {name: "Maksim", age: 23 },
//     {name: "Peter", age: 9 },
// ]
// let result = myArray.find(function (item, index, array)
// {    return item.age === 18; })
// тоже самое стрелочная фун-я:
// let result = myArray.find(item => item.age === 18);

// let result = myArray.filter(function (item, index, array)
//  {    return item.age >= 18; })
// console.log(result);

//сортировка чисел
// let arrNumber = [2, 77, 18, 10, 5];
// console.log(arrNumber.sort());

// function sortNum(a, b) {
//     console.log(`sorting ${a} and ${b}`);
//     if (a>b) return 1;
//     if (a==b) return 0;
//     if (a<b) return -1;
// }

// то-же самое
// function sortNum(a, b) {
//     console.log(`sorting ${a} and ${b}`);
//     return a - b;
// }

// console.log(arrNumber.sort(sortNum));

// то-же самое стрелочной функцией одной строкой:
// console.log(arrNumber.sort((a, b) => a - b));




/**
 * Задача 1.
 *
 * Дан массив с днями недели.
 *
 * Вывести в консоль все дни недели с помощью:
 * - Классического цикла for;
 * - Цикла for...of;
 * - Специализированного метода для перебора элементов массива forEach.
 */

/* Дано
const days = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
];
for (let i=0; i<days.length; i++) {
    console.log(days[i])
}
for (let key of days) {
    console.log(key)
}
days.forEach(function (key) {
    console.log(`${key}`) })
*/




// Создать массив чисел от 1 до 100.
// Отфильтровать его таким образом, чтобы в новый массив не попали числа меньше 10 и больше 50.
// Вывести в консоль новый массив.
// let number = [];
// for (let i = 0; i<100; i++) {
//     number.push(i)
// }
// let numberNew = number.filter(number=> number>10 && number<50 );
// console.log(numberNew)


// Есть массив брендов автомобилей ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам нужно получить новый массив, объектов типа
// {
//     type: 'car'
//     brand: ${элемент массива}
// }
// Вывести массив в консоль

// let car = ["bMw", "Audi", "teSLa", "toYOTA"];
// let carNew = car.map(item => {
//     return { type: 'car',
//             brand: item.toLowerCase(),}
// })
// console.log(carNew)
//
// // тоже самое через forEach
// const newArray1 = [];
// car.forEach(item => {
//     newArray1.push({
//         "type": "car",
//         "brand": item.toUpperCase(),
//     })
// })
//
// console.log(newArray1);






// Для всех машин добавить свойство isElectric, для теслы оно будет true, для остальных false;
// Для того чтоб проверять что машина электрическая создать отдельную функцию что будет хранить
// в себе массив брендов электрических машин и будет проверять входящий элемент - на то,
// если он в массиве.

// Создать новый массив, в котором хранятся все машины, что не являются электрокарами.
// Вывести его в модальное окно пользователю.

// const checkIsElectric = name => {
//     let electricCars = ["TESLA"]
//     return electricCars.includes(name.toUpperCase())
// }
//
// const createArray = array => {
//     const newArray = array.map(item=> ({type:"car",
//         brand: item.toUpperCase(),
//         isElectric: checkIsElectric(item) }))
//     return newArray.filter(car => car.isElectric===false)
// }
// console.log(createArray(array))



// Создать массив объектов students (в количестве 7 шт).
// У каждого студента должно быть имя, фамилия и направление обучения - Full-stask
// или Front-end. У каждого студента должен быть метод, sayHi, который возвращает строку
// `Привет, я ${имя}, студент Dan, направление ${направление}`.
// Перебрать каждый объект и вызвать у каждого объекта метод sayHi;

// const students = [
//     {   name: 'Ann',
//         lastName: 'Marela',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`)
//         }
//     },
//     {   name: 'John',
//         lastName: 'Lennon',
//         major: 'Full-stask',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`)
//         }
//         },
//
//     {   name: 'Eric',
//         lastName: 'Claptone',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`)
//         }
//         },
//
//     {   name: 'Riki',
//         lastName: 'Martin',
//         major: 'Front-End',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`) }
//         },
//
//     {   name: 'Elton',
//         lastName: 'John',
//         major: 'Full-stask',
//         sayHi() {
//             console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`) }
//         },
//
// ]
// students.forEach(students=> {
//     students.sayHi();
// })


// students.forEach(student => {
//     student.sayHi = function () {
//         console.log(`Привет, я ${this.name}, студент Dan, направление ${this.major}`);
//     };
// })
//


/**
 *  Дан список игроков. Нужно отсортировать массив по убыванию, и добавить каждому игроку поле place,
 *  в котором будет указано
 *  его место в списке игроков - то есть Иван должен оказаться на первом месте, Василий - на последнем
 * */

//  const users = [
//     { name: 'Victor', score: 20 },
//     { name: 'Mario', score: 10 },
//     { name: 'Tatiana', score: 30 },
//     { name: 'Vasiliy', score: 0 },
//     { name: 'Ivan', score: 100 },
// ];
// const array = [
//     {total: 1},
//     {total: 2},
//     {total: 3},
// ]
// users.sort((a, b) => b.score - a.score);
// const usersPlaces = users.map((item, index)=>) {
// return {...item, place: index +1}
// }





/* Задача 1.  Напишите функцию sumInput(), которая:

Просит пользователя ввести значения, используя prompt и сохраняет их в массив.
    Заканчивает запрашивать значения, когда пользователь введёт не числовое значение, пустую строку или нажмёт «Отмена».
Подсчитывает и возвращает сумму элементов массива.
    P.S. Ноль 0 – считается числом, не останавливайте ввод значений при вводе «0».

function sumInput () {
    const accumulator = [];
    let userData;
    do {
        userData = +prompt("Enter Number");
        if (userData) {
            accumulator.push(userData) ;} else {
            break
        };
    } while (!isNaN(userData))

    accumulator.forEach((item)=>{
        userData = userData + item;
    })
    alert(`Your sum = ${userData}`)}
sumInput()
*/

/* Задача 2. Написать мульти-язычную программу-органайзер.
 *
 * Программа спрашивает у пользователя на каком языке он желает увидеть список дней недели.
 * После ввода пользователем желаемого языка программа выводит в консоль дни недели на указанном языке.
 *
 * Доступные языки (локали): ua, ru, en.
 *
 * Если введённая пользователем локаль не совпадает с доступными — программа переспрашивает его до тех пор,
 * пока доступная локаль не будет введена.
 *
 * Условия:
 * - Решение должно быть коротким, лаконичным и универсальным;
 * - Всячески использовать методы массива;
 * - Использование метода Object.keys() обязательно.

 //  Дано
 const days = {
    ua: [
        'Понеділок',
        'Вівторок',
        'Середа',
        'Четвер',
        'Пятниця',
        'Субота',
        'Неділя',
    ],
    ru: [
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота',
        'Воскресенье',
    ],
    en: [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday',
    ],
};
 let local = null;
 do {
    local = prompt("Enter local lang (en,ua,ru)")
} while (local !=='ua' && local !=='ru' && local !=='en' )

console.log( Object.values(days[local]))
// alert( Object.values(days[local]))
*/


/*
Методы Array
Объект Array имеет следующие методы:

concat() объединяет два массива и возвращает новый массив.

var myArray = new Array("1", "2", "3");
myArray = myArray.concat("a", "b", "c");
// myArray = "1", "2", "3", "a", "b", "c" 

join(deliminator = ',') объединяет элементы массива в текстовую строку.
var myArray = new Array("Wind", "Rain", "Fire");
var list = myArray.join(" - "); // list = "Wind - Rain - Fire"

push() добавляет один или несколько элементов в конец массива и возвращает результирующую длину.
var myArray = new Array("1", "2");
myArray.push("3"); // myArray ="1", "2", "3" 

pop() удаляет из массива последний элемент и возвращает его.
var myArray = new Array("1", "2", "3");
var last = myArray.pop();
// myArray ="1", "2" , last = "3"

shift() удаляет из массива первый элемент и возвращает его.
var myArray = new Array ("1", "2", "3");
var first = myArray.shift();
// myArray = "2", "3" , first = "1"

unshift() добавляет один или несколько элементов в начало массива и возвращает его новую длину.
var myArray = new Array ("1", "2", "3");
myArray.unshift("4", "5");

slice(start_index, upto_index) возвращает секцию массива как новый массив.
var myArray = new Array ("a", "b", "c", "d", "e");
myArray = myArray.slice(1, 4); 
                               
splice(index, count_to_remove, addElement1, addElement2, ...) удаляет часть элементов из массива и (опционально) заменяет их. Возвращает удалённые элементы.
var myArray = new Array ("1", "2", "3", "4", "5");
myArray.splice(1, 3, "a", "b", "c", "d");


reverse() переставляет элементы массива в обратном порядке: первый элемент становится последним, а последний - первым.
var myArray = new Array ("1", "2", "3");
myArray.reverse();

sort() сортирует элементы массива.
var myArray = new Array("Wind", "Rain", "Fire");
myArray.sort();

Метод sort() может принимать в качестве аргумента callback-функцию, которая определяет каким образом сравнивать элементы массива при сортировке. Функция сравнивает два значения, и возвращает одно из трёх значений (список вариантов значений смотрите после примера):

Пример. Следующий код сортирует массив по последнему символу в строке:

var sortFn = function(a, b){
  if (aa.length - 1  < bb.length - 1 ) return -1;
  if (aa.length - 1  > bb.length - 1 ) return 1;
  if (aa.length - 1  == bb.length - 1 ) return 0;
}
myArray.sort(sortFn);
если a меньше чем b в выбранной системе сравнения, возвращаем -1 (или любое отрицательное число)
если a больше чем b в выбранной системе сравнения, возвращаем 1 (или любое положительное число)
если a и b считаются равными, возвращаем 0.
indexOf(searchElement, fromIndex ) ищет в массиве элемент со значением searchElement и возвращает индекс первого совпадения.

var a = 'a', 'b', 'a', 'b', 'a' ;
console.log(a.indexOf('b')); 

console.log(a.indexOf('b', 2)); 
console.log(a.indexOf('z')); 
lastIndexOf(searchElement, fromIndex ) тоже самое, что и indexOf, но поиск ведётся в обратном порядке, с конца массива.

var a = 'a', 'b', 'c', 'd', 'a', 'b' ;
console.log(a.lastIndexOf('b')); 

console.log(a.lastIndexOf('b', 4)); 
console.log(a.lastIndexOf('z')); 
forEach(callback, thisObject ) выполняет callback-функцию по каждому элементу массива.

var a = 'a', 'b', 'c' ;
a.forEach(function(element) { console.log(element);} );

map(callback, thisObject ) возвращает новый массив, содержащий результаты вызова callback-функции для каждого элемента исходного массива.

var a1 = 'a', 'b', 'c' ;
var a2 = a1.map(function(item) { return item.toUpperCase(); });
console.log(a2); 
filter(callback, thisObject ) возвращает новый массив, содержащий только те элементы исходного массива, для которых вызов callback-функции вернул true.

var a1 = 'a', 10, 'b', 20, 'c', 30 ;
var a2 = a1.filter(function(item) { return typeof item == 'number'; });
console.log(a2); 
every(callback, thisObject ) возвращает true, если вызов callback-функции вернул  true для всех элементов массива.

function isNumber(value){
  return typeof value == 'number';
}
var a1 = 1, 2, 3 ;
console.log(a1.every(isNumber)); 
var a2 = 1, '2', 3 ;
console.log(a2.every(isNumber)); 
some(callback, thisObject ) возвращает true, если вызов callback-функции вернёт true хотя бы для одного элемента.

function isNumber(value){
  return typeof value == 'number';
}
var a1 = 1, 2, 3 ;
console.log(a1.some(isNumber)); 
var a2 = 1, '2', 3 ;
console.log(a2.some(isNumber)); 
var a3 = '1', '2', '3' ;
console.log(a3.some(isNumber)); */



// ===============================
// var colors = ['red', 'green', 'blue' ];
// colors.forEach(function(color) {
//   console.log(color);
// }); // перебор массива forEach
// // сокращенно colors.forEach(color => console.log(color));
//
//
//
// let fruits = ["Яблоко", "Апельсин", "Слива"] ;
// // проходит по значениям
// for (let fruit of fruits) {
//   console.log( fruit );
// }
// Цикл for..of не предоставляет доступа к номеру текущего элемента, только к его значению, но в большинстве случаев этого достаточно. А также это короче.






// Описание задачи: Напишите функцию, которая очищает массив от нежелательных значений,
// таких как false, undefined, пустые строки, ноль, null.

// const data = [0, 1, false, 2, undefined, '', 3, null];
//
// const compact = () => data.filter(item => !!item);
// const compact1 = () => data.filter(item => Boolean(item));
// const compact2 = () => data.filter(item => item === true);
//
// console.log(compact(data)) // [1, 2, 3]
// console.log(compact1(data)) // [1, 2, 3]
// console.log(compact2(data)) // [1, 2, 3]
