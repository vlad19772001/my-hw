"use strict";
let myObject = {}; // создать пустой объект
myObject.name = "John"; // добавить в объект свойство+значение
myObject.surname = "Smith"; // добавить в объект свойство+значение
myObject["dva slova"] = "Smith two"; // добавить в объект свойство+значение
myObject.name = "Peter"; // поменять значение на новое
delete myObject.name; // удалить значение
// console.log(myObject);
// console.log(myObject["dva slova"]); // 

// Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.
function isEmpty (obj) {
    for (let key in obj) {  // если тело цикла начнет выполняться - значит в объекте есть свойства
        return false
    }
    return true
}
let schedule = {};
// alert( isEmpty(schedule) ); // true
schedule["8:30"] = "get up";
// alert( isEmpty(schedule) ); // false

let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130,
  }
// Напишите код для суммирования всех зарплат и сохраните результат в переменной sum. Должно получиться 390.
// Если объект salaries пуст, то результат должен быть 0.
let sum = 0;
for (let key in salaries) { // т.е. для каждого ключа суммируем его значение в "СУМ"
    sum += salaries[key]
} 
// console.log(sum);

// salaries = {
//     John: 100,
//     Ann: 160,
//     Pete: 130,
//     title: "My menu",
//   }
//   console.log(salaries);
//   for (let key in salaries) { // т.е. для каждого ключа 
//     if (typeof salaries[key] === "number" ) { // который число
//         salaries[key] = salaries[key]*2; // salaries[key] *=2
//     } else {
        
//     }    
// } 
// console.log(salaries);




// class work
// const myObj = {
// 	title: 'cucumber',
// 	price: 10,
// 	sort: '...',
// 	'sort asc': 213,
// };
// const keyObject = prompt('Enter key for delete'); // price
// const secondKey = 'sort';
// myObj[keyObject];
// myObj.keyObject;



// const myHelper = 'ds';

// const myAnimal = {
// 	title: 'Барсик',
// 	makeNoise: function () {
// 		alert(`${this.title} Гав`);
// 	},
// 	subObj: {
// 		age: '4',
// 	},
// };

// for (let key in myAnimal) {
// 	console.log(key);
// 	console.log(myAnimal[key]);
// }

// 2
// Вывести все размеры key (SIZE) ноута используя for..in

// const notebook = {
// 	name: 'Name',
// 	price: 34000,
// 	size: {
// 		width: 15,
// 		height: 10,
// 	},
// };

// for (let key in notebook.size) {
// 	console.log(notebook.size[key]);
// }

// // 3
// // создать пустой объект notebook2 и скопировать все поля из объекта notebook. Поменять поле name на другое

// const notebook2 = {};

// for (let key in notebook) {
// 	if (key === 'size') {
// 		notebook2[key] = {};
// 		for (let key2 in notebook[key]) {
// 			notebook2[key][key2] = notebook[key][key2];
// 		}
// 	} else {
// 		notebook2[key] = notebook[key];
// 	}
// }

// notebook2.name = 'Tet';
// notebook2.size.width = 20;

// console.log(notebook, notebook2);

// const testObj = {
// 	er: {
// 		sfa: 34,
// 	},
// };

// 4
// Спросить у пользователя имя, фамилию и хочет ли он кушать.
// Проверить, что пользователь что-то ввел при вводе имени или фамилии.
// Иначе - переспрашивать.
// Записать полученные данные в объект Person.
// В объекте создать 2 метода - eat, задача которого выводить алерт с текстом
// "Пойду поем", go, задача которого выводить алерт с текстом "Тогда я пошел".

// Если пользователь указал, что хочет кушать, то вызвать метод eat, иначе - метод go.

// let name = "";
// let lastName = ""; 
// const person = {
// 	name : prompt("Enter Name"),
// 	lastName : prompt("Enter Lastname"),
// 	eat : ()=>alert("Пойду поем"),
// 	go : ()=>alert("Тогда я пошел"),
// }
// console.log(person);
// if (confirm('Do you want to eat?')) {
// 	person.eat();
// } else {
// 	person.go();
// }


// let name = null;
// let hungry = null;
// do {
// 	name = prompt('Enter name');
// } while (!name);
// hungry = confirm('Do you want to eat?');

// const person = {
// 	name: name,
// 	hungry: hungry,
// 	eat: function () {
// 		alert('Пойду поем');
// 	},
// 	go: function () {
// 		alert('Тогда я пошел');
// 	},
// };
// if (hungry) {
// 	person.eat();
// } else {
// 	person.go();
// }

// 5
// Создать объект danItStudent,
// у которого будут такие свойства: имя, фамилия, количество сданных домашних работ.
// Спросить у пользователя "Что вы хотите узнать о студенте?"
// Если пользователь ввел "name" или "имя" - вывести в консоль имя студента.
// Если пользователь ввел "lastName" или "фамилия" - вывести в консольфамилию студента.
// Если пользователь ввел "оценка" или "homeworks" - вывести в консоль количество сданных работ.
// Если ввел, что-то не из перечисленого - вывести в консоль - "Извините таких данных нету"

// let danItStudent = {
// 	name: 'Bill',
// 	lastName: 'Gates',
// 	homeWorks: 5,
// };

// let user = prompt('Что вы хотите узнать о студенте?');
// if (user === 'name' || user === 'имя') {
// 	console.log(danItStudent.name);
// } else if (user === 'lastName' || user === 'фамилия') {
// 	console.log(danItStudent.lastName);
// } else if (user === 'оценка' || user === 'homeworks') {
// 	console.log(danItStudent.homeWorks);
// } else {
// 	console.log('Извините таких данных нету');
// }

// const danItStudentCopy = Object.assign({}, danItStudent);

// task from DanIt-Book
// const student = {
// 	name: "John",
// 	"last name": "Smith",
// 	laziness : 2,
// 	trick: 1,
// }
// console.log(student);
// let product = {
// 	name : "notebook", 
// 	"full name" : "Ноутбук Acer Swift 3 SF314-52", 
// 	price : 800, 
// 	availability: false,
// 	"additional gift" : null,
// } 
// console.log(product);
// if (student.laziness<=3 || student.trick<=4) {
// 	console.log(`Студент ${student.name} ${student["last name"]} передан в военкомат`);
// }
//Задача 5 Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Спросить у пользователя "Что вы хотите узнать о студенте?" и вывести в консоль эту информацию.
// let danItStudent = {
// 	name: "John",
// 	age : 25,
// 	sex: "man",
// 	"last name": "Smith",
// 	"finished home work" : 7,
// }
// let request = prompt("what do you know? (name,age...)")
// for(let key in danItStudent) {
// 	if (key===request) {console.log(danItStudent[key]);}
// }
// console.log(danItStudent[request]); // тоже самое без иф и перебора
// console.log(danItStudent[prompt("what do you know? (name,age...)")]); // тоже самое working


// Задача 6
// Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Вывести объект в консоль. После чего сначала спросить у пользователя "Какое свойство вы хотите изменить?", потом - "На какое значение?". Изменить требуемое свойство и вывести объект в консоль.
// let request = prompt("Какое свойство вы хотите изменить? (name,age...)")
// let newProperty = prompt("На какое значение")
// for (let key in danItStudent) {
// 	if (key === request) {
// 		danItStudent[key] = newProperty;
// 	}
// }
// console.log(danItStudent);

// Задача 7
// Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Вывести объект в консоль. После чего спрашивать у пользователя в цикле два вопроса - "Какое свойство вы хотите изменить?", потом - "На какое значение?". Цикл может иметь максимум 3 итерации. Если пользователь нажмет Cancel на любой из вопросов - досрочно прервать цикл. Вывести объект в консоль

/*let request, newProperty = null;

for (let i=0; i<3; i++ ) {
request = prompt("Какое свойство вы хотите изменить? (name,age...)");
newProperty = prompt("На какое значение");
if (!request || !newProperty) {
  break
} 

	danItStudent[request] = newProperty;
} ;

console.log(danItStudent); */

// Задача 8
// Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Вывести объект в консоль. После чего спросить у пользователя "Какое свойство вы хотите изменить?", и если у объекта такого свойства нет - переспрашивать с сообщением "имяСвойства у объекта нет. Напишите, пожалуйста, правильное свойство объекта" пока пользователь не введет имя существующего свойства объекта или не нажмет Cancel. Если пользователь не нажал Cancel, спросить "Какое значение вы хотите присвоить этому свойству?". Изменить требуемое свойство и вывести объект в консоль.
// console.log(danItStudent);
// let request = prompt("Какое свойство вы хотите изменить? (name,age...)");
// while (!(request in danItStudent)) {
//        request = prompt(`${request} у объекта нет. Напишите, пожалуйста, правильное свойство объекта`);
//     if (request === null) {
//     break;
//     };
// }
// if (request !== null) {
//   let newProperty = prompt("На какое значение");
//   danItStudent[request] = newProperty;
// }
// console.log(danItStudent);

// Задача 9
/* Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Вывести объект в консоль. После чего спрашивать у пользователя в цикле:

"Какое свойство вы хотите изменить?"
Если у объекта такого свойства нет - переспрашивать с сообщением "имяСвойства у объекта нет. Напишите, пожалуйста, правильное свойство объекта" пока пользователь не введет существующее свойство или не нажмет Cancel.
Если пользователь нажал Cancel - завершить цикл переспрашивания и завершить цикл вопросов, выведя объект в консоль.
Если пользователь ввел-таки имя существующего свойства объекта, задать вопрос - "На какое значение вы хотите поменять имяСвойства?".
Поменять указанное свойство и начать новую итерацию цикла.
Цикл может иметь максимум 3 итерации. Если пользователь нажмет Cancel на любой из вопросов - досрочно прервать цикл. Вывести объект в консоль. */

// console.log(danItStudent);
// for (let i=0; i<3; i++ ) {
// let request = prompt("Какое свойство вы хотите изменить? (name,age...)");
// while (!(request in danItStudent)) {
//        request = prompt(`${request} у объекта нет. Напишите, пожалуйста, правильное свойство объекта`);
//     if (request === null) {
//       break;
//     };
// }
// if (request !== null) {
//   let newProperty = prompt("На какое значение");
//   danItStudent[request] = newProperty;
// }}
// console.log(danItStudent);


// Задача 10
// Скопируйте объект student из задачи 1, напишите после его создания такой код: если коэффициент лени больше или равен 3, и при этом меньше или равен 5, а коэффициент хитрости больше или равен 4 - добавьте объекту свойство "new status" со значением "Студент имяСтудента фамилияСтудента отправлен на пересдачу". Выведите объект в консоль.
// console.log(student);
// student.laziness = 4;
// student.trick = 4;
// if (student.laziness>=3 && student.laziness<=5 && student.trick>=4) {
//   student["new status"] = `Студент ${student.name} ${student["last name"]} отправлен на пересдачу`
// }
// console.log(student);

/* Создать пустой объект student2, спросить его имя и фамилию, добавить их как свойства объекта, и вывести в консоль сообщение "Студент имяСтудента фамилияСтудента готов к заполнению табеля!". После чего в цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Если он ввел название очередного предмета и оценку по нему - добавить название как свойство объекта и оценку - как его значение. По окончании цикла вывести объект student консоль. */

// let student2 = {};
// let name = prompt("Enter Name");
// let lastName = prompt("Enter Last Name")
// if (name !=="" && lastName!=="") {
//   student2.name = name;
//   student2.lastName = lastName;
//   console.log(`Студент ${name} ${lastName} готов к заполнению табеля!`);
// }
// while (true) {
//   let course = prompt("Введи предмет")   
//   if (!course) {
//     break;
//   }
//   let ball = prompt(`Введи оценку по ${course}`) 
//   student2[course] = ball;

// }
// console.log(student2);

// Задача 13
/* Создайте объект tabel со свойствами в виде названия трех разных предметов, значения которых пока равно null. Пример:

const tabel = {
  history: null,
  biology: null,
  javascript: null
}
Выведите объект в консоль, после чего спросите у пользователя оценки по каждому предмету. Если полученная оценка ниже 4, удалите это свойство из объекта tabel. По оконанию выведите объект в консоль.

const tabel = {
  history: null,
  biology: null,
  javascript: null
};

console.log(tabel);

const historyGrade = +prompt("Ваша оценка по истории");

if (historyGrade < 4) {
  delete tabel.history;
} else {
  tabel.history = historyGrade;
}

const biologyGrade = +prompt("Ваша оценка по истории");

if (biologyGrade < 4) {
  delete tabel.biology;
} else {
  tabel.biology = biologyGrade;
} 

const javascriptGrade = +prompt("Ваша оценка по истории");

if (javascriptGrade < 4) {
  delete tabel.javascript;
} else {
  tabel.javascript = javascriptGrade;
}

console.log(tabel); */

// Задача 14
/* Посчитайте количество свойств объекта:

const user = { 
  name: "Влад", 
  "second name": "Дракула", 
  age: 400, 
  marriage: false 
};
и добавьте ему свойство length, равное количеству свойств объекта. Выведите объект в консоль. */

// let studLenhgt = 0;
// for (let key in student2) {
//   studLenhgt++;
// }
// console.log(studLenhgt);

/* Задача 15
Создайте пустой объект casesList. В цикле спрашивайте у пользователя:

название дела;
затраты времени на это дело (в минутах);
Если пользователя нажал Cancel - выйдите из цикла и выведите объект в консоль, после чего подсчитайте общее количество дел. Если их больше 10 - выведите сообщение "Кто-то оптимист". А если общее количество времени на все дела больше 16 часов - также выведите сообщение "И очень хочет быть похожим на Цезаря".


const casesList = {};
let caseName = prompt("Ввведите название дела");
let caseDuration = +prompt("Ввведите продолжительность дела в минутах");

while (caseName && caseDuration) {
  casesListcaseName  = caseDuration;
  caseName = prompt("Ввведите название дела");
  caseDuration = +prompt("Ввведите продолжительность дела в минутах");
}

console.log(casesList);

let casesCount = 0;
let allCasesDuration = 0;
for (let key in casesList) {
  casesCount++;
  allCasesDuration += casesListkey ;
}

if (casesCount > 10) {
  console.log("Кто-то оптимист ");
}

if ((allCasesDuration / 60) > 16) {
  console.log("И очень хочет быть похожим на Цезаря");
} */

/*Задача 16
Создать пустой объект tabel, после чего в цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. После чего посчитать количество плохих (меньше 4) оценок по предметам. Если их больше двух - вывести в консоль сообщение "Студент НЕ переведен на следующий курс".

Нажмите, чтобы подсмотреть решение

const tabel = {};
let course = prompt("Введите название предмета");
let courseGrade = +prompt("Введите оценку по предмету");

while (course) {
  tabelcourse  = courseGrade;
  course = prompt("Введите название предмета");
  courseGrade = +prompt("Введите оценку по предмету");
}

let badGrades = 0;

for(let key in tabel) {
  if (tabelkey  < 4) {
    badGrades++;
  }
}

if (badGrades > 2) {
  console.log("Студент НЕ переведен на следующий курс");
}
*/


/*Задача 17
Создать пустой объект tabel, после чего в цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. После чего посчитать количество плохих (меньше 4) оценок по предметам. Вывести объект tabel в консоль Если плохих оценок больше двух - вывести в консоль сообщение "Студент НЕ переведен на следующий курс". После чего создать пустой объект retakeCourses, в который добавляете те предметы, по которым оценка у студента меньше 4, со значением null, и удаляете это свойство у объекта tabel. Вывести оба объекта (tabel и retakeCourses) в консоль.

Нажмите, чтобы подсмотреть решение

const tabel = {};
let course = prompt("Введите название предмета");
let courseGrade = +prompt("Введите оценку по предмету");

while (course) {
  tabelcourse  = courseGrade;
  course = prompt("Введите название предмета");
  courseGrade = +prompt("Введите оценку по предмету");
}

let badGrades = 0;
for (let key in tabel) {
  if (tabelkey  < 4) {
    badGrades++;
  }
}

let retakeFlag = false;
const retakeCourses = {};
if (badGrades > 2) {
  console.log("Студент НЕ переведен на следующий курс");
  retakeFlag = true;

  for (let key in tabel) {
    if (tabelkey  < 4) {
      retakeCourseskey  = null;
      delete tabelkey ;
    }
  }
}

console.log(tabel);

if(retakeFlag) {
  console.log(retakeCourses);
}
*/


/* Задача 18
Создать пустой объект tabel, после чего в цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. После чего посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение "Студент переведен на следующий курс". После чего посчитать средний балл по предметам, и если он больше 7 - вывести сообщение "Студенту назначена стипендия".

Нажмите, чтобы подсмотреть решение

const tabel = {};
let course = prompt("Введите название предмета");
let courseGrade = +prompt("Введите оценку по предмету");

while (course) {
  tabelcourse  = courseGrade;
  course = prompt("Введите название предмета");
  courseGrade = +prompt("Введите оценку по предмету");
}

let badGrades = false;

for (let key in tabel) {
  if (tabelkey  < 4) {
    badGrades = true;
    break;
  }
}

if (!badGrades) {
  console.log("Студент переведен на следующий курс");
  let averageGrade = 0;
  let courseCont = 0;

  for (let key in tabel) {
    courseCont++;
    averageGrade +=tabelkey ;
  }

  averageGrade = averageGrade/courseCont;

  if (averageGrade > 7) {
    console.log("Студенту назначена стипендия");
  }
}
*/


// ---- Object.method 

// Задача 4
// Создайте объект notebook с полями: name и price, и добавьте встроеный метод getDiscount(), в который в качестве аргумента будет передаваться процент скидки, и который будет возвращать цену с учетом этой скидки. Спросите у пользователя размер скидки и выведите в консоль цену с учетом этой скидки.
// const notebook = {
// 	name: 'Name',
// 	price: 34000,
// 	size: {
// 		width: 15,
// 		height: 10,
// 	},
//   getDiscount: function (discount) {
//     return this.price - discount/100*this.price;
//   }

// };
// let skidka = +prompt("Enter % off")
// console.log(notebook.getDiscount(skidka));
// for (let key in notebook.size) {
// 	console.log(notebook.size[key]);
// }

// Задача 5
// Возмите объект student: и добавьте в него функцию averageRating, которая выводит средний бал оценок по предметам. Выведите средний бал в консоль.

// const student = {
//   name: "Катерина",
//   "last name": "Петрова",
//   status: null,
//   tabel: {
//     history: 12,
//     biology: 12,
//     mathematics: 8,
//     physics: 9,
//     geography: 7
//   },
//   averageRating : function () {
//     let rating=0;
//     for (let key in this.tabel) {
//       rating += this.tabel[key];
//     }
//     return rating
//   }
// };
// console.log(student.averageRating());

