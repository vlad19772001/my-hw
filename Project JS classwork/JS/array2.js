"use strict";
// ++++++++++++++++++++ ЗАДАЧИ
/* 
1. Переведите текст вида border-left-width в borderLeftWidth
Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» в «myShortString».
То есть дефисы удаляются, а все слова после них получают заглавную букву.

Примеры:
camelize("background-color") == 'backgroundColor';
camelize("list-style-image") == 'listStyleImage';
camelize("-webkit-transition") == 'WebkitTransition';
P.S. Подсказка: используйте split, чтобы разбить строку на массив символов, потом переделайте всё как нужно и методом join соедините обратно. 
function camelize(text) {
    return text
    .split('-')
    .map(
        (word, index) => index === 0 ? word : word = word[0].toUpperCase() + word.slice(1)
    )
    .join('')
}
console.log(camelize('list-style-image'));
*/




/*
2. Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет в нём элементы между a и b и отдаёт массив этих элементов.

Функция должна возвращать новый массив и не изменять исходный.

Например:
let arr = [5, 3, 8, 1];
let filtered = filterRange(arr, 1, 4);
alert( filtered ); // 3,1 (совпадающие значения)
alert( arr ); // 5,3,8,1 (без изменений) 
let arrTask = [5, 3, 8, 1, 7, 12, 20];
function filterRange(arr, a, b) {
    return arr.filter(item => (item >=a && item <= b));
}

console.log(filterRange(arrTask,5,15));
*/



/*
3. Напишите функцию filterRangeInPlace(arr, a, b), которая принимает массив arr и удаляет из него все значения кроме тех, которые находятся между a и b. То есть, проверка имеет вид a ≤ arr[i] ≤ b.

Функция должна изменять принимаемый массив и ничего не возвращать.

Например:*/
// let arr = [5, 3, 8, 1];
// filterRangeInPlace(arr, 1, 4); // удалены числа вне диапазона 1..4
// alert( arr ); // [3, 1] 
// let arrTask = [5, 3, 8, 1, 7, 12, 20];
// function filterRangeInPlace(arr, a, b) {
//     for (let i=0; i<arr.length; i++) {
//         let value = arr[i]
//         if (value <a || value>b) {
//             arr.splice(i, 1);
//             i--;
//         }
//     }
// }
//
// filterRangeInPlace(arrTask,5,15);
// console.log(arrTask);
//

/*
4. Сортировать в порядке по убыванию
важность: 4
let arr = [5, 2, 1, -10, 8];
// ... ваш код для сортировки по убыванию
alert( arr ); // 8, 5, 2, 1, -10   */
// let arr = [5,77, 2,31, 12, -10, 8];
// arr.sort((a,b)=>a-b)
// console.log(arr);

/*
Скопировать и отсортировать массив
У нас есть массив строк arr. Нужно получить отсортированную копию, но оставить arr неизменённым.
Создайте функцию copySorted(arr), которая будет возвращать такую копию.
let arr = ["HTML", "JavaScript", "CSS"];
let sorted = copySorted(arr);
alert( sorted ); // CSS, HTML, JavaScript
alert( arr ); // HTML, JavaScript, CSS (без изменений)*/
// let arr = ["HTML", "JavaScript", "CSS"];
// function copySorted(massiv) {
//     return massiv.slice().sort()
// }
// let sorted = copySorted(arr);
// alert( sorted ); // CSS, HTML, JavaScript
// alert( arr ); //HTML, JavaScript, CSS (без изменений)



/* 5.  Создать расширяемый калькулятор

Создайте функцию конструктор Calculator, которая создаёт «расширяемые» объекты калькулятора.
Задание состоит из двух частей.

Во-первых, реализуйте метод calculate(str), который принимает строку типа "1 + 2" в формате «ЧИСЛО оператор ЧИСЛО» (разделено пробелами) и возвращает результат. Метод должен понимать плюс + и минус -.

Пример использования:

let calc = new Calculator;

alert( calc.calculate("3 + 7") ); // 10
Затем добавьте метод addMethod(name, func), который добавляет в калькулятор новые операции. Он принимает оператор name и функцию с двумя аргументами func(a,b), которая описывает его.

Например, давайте добавим умножение *, деление / и возведение в степень **:

let powerCalc = new Calculator;
powerCalc.addMethod("*", (a, b) => a * b);
powerCalc.addMethod("/", (a, b) => a / b);
powerCalc.addMethod("**", (a, b) => a ** b);

let result = powerCalc.calculate("2 ** 3");
alert( result ); // 8
Для этой задачи не нужны скобки или сложные выражения.
Числа и оператор разделены ровно одним пробелом.
Не лишним будет добавить обработку ошибок. */

// function Calculator() {

//     this.methods = {
//       "-": (a, b) => a - b,
//       "+": (a, b) => a + b
//     };
  
//     this.calculate = function(str) {
  
//       let split = str.split(' '),
//         a = +split[0],
//         op = split[1],
//         b = +split[2]
  
//       if (!this.methods[op] || isNaN(a) || isNaN(b)) {
//         return NaN;
//       }
  
//       return this.methods[op](a, b);
//     }
  
//     this.addMethod = function(name, func) {
//       this.methods[name] = func;
//     };
//   }

//   let calc = new Calculator;

// alert( calc.calculate("3 + 7") ); // 10
// let powerCalc = new Calculator;
// powerCalc.addMethod("*", (a, b) => a * b);
// powerCalc.addMethod("/", (a, b) => a / b);
// powerCalc.addMethod("**", (a, b) => a ** b);

// let result = powerCalc.calculate("2 ** 3");
// alert( result ); // 8





/* 6. Трансформировать в массив имён
У вас есть массив объектов user, и в каждом из них есть user.name. Напишите код, который преобразует их в массив имён.
Например:
let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 28 };
let users = [ vasya, petya, masha ];
let names = /* ... ваш код */
// alert( names ); // Вася, Петя, Маша 

// let vasya = { name: "Вася", age: 25 };
// let petya = { name: "Петя", age: 30 };
// let masha = { name: "Маша", age: 28 };

// let users = [ vasya, petya, masha ];

// let names = users.map(item => item.name);

// alert( names ); // Вася, Петя, Маша



/* 7. Трансформировать в объекты
У вас есть массив объектов user, и у каждого из объектов есть name, surname и id.

Напишите код, который создаст ещё один массив объектов с параметрами id и fullName, где fullName – состоит из name и surname.

*/
// let vasya = { name: "Вася", surname: "Пупкин", id: 1 };
// let petya = { name: "Петя", surname: "Иванов", id: 2 };
// let masha = { name: "Маша", surname: "Петрова", id: 3 };

// let users = [ vasya, petya, masha ];
// console.log(users);
// // для каждого юзера делаем функцией (создать объект с двумя свойствами:) 
// let usersMapped = users.map(item => ({
//     fullName: `${item.name} ${item.surname}`,
//     id: item.id,
// }) )
// console.log(usersMapped);




/* 8. Отсортировать пользователей по возрасту

Напишите функцию sortByAge(users), которая принимает массив объектов со свойством age и сортирует их по нему.
*/
// let vasya = { name: "Вася", age: 25 };
// let petya = { name: "Петя", age: 30 };
// let masha = { name: "Маша", age: 28 };

// let arr = [ vasya, petya, masha ];
// function sortByAge(massiv) {
//     massiv.sort((a,b)=>(a.age-b.age))
// }

// sortByAge(arr);
// console.log(arr);

// теперь: [vasya, masha, petya]
// alert(arr[0].name); // Вася
// alert(arr[1].name); // Маша
// alert(arr[2].name); // Петя

/* 9. Получить средний возраст
Напишите функцию getAverageAge(users), которая принимает массив объектов со свойством age и возвращает средний возраст.
Формула вычисления среднего арифметического значения: (age1 + age2 + ... + ageN) / N.
*/
// let vasya = { name: "Вася", age: 25 };
// let petya = { name: "Петя", age: 30 };
// let masha = { name: "Маша", age: 29 };

// let arr = [ vasya, petya, masha ];
// function getAverageAge(massiv) {
//     let average = 0;
//     for (let key of massiv) {
//         console.log(key.age);
//         average = average +key.age;
//     }
//     console.log(average);
//     console.log(massiv.length);

//     return average/massiv.length;
// }

// alert( getAverageAge(arr) ); // (25 + 30 + 29) / 3 = 28
// // моё решение работает, а в учебнике так:
// function getAverageAge(users) {
//     return users.reduce((prev, user) => prev + user.age, 0) / users.length;
//   }

/* 10. Оставить уникальные элементы массива

Пусть arr – массив строк.
Напишите функцию unique(arr), которая возвращает массив, содержащий только уникальные элементы arr. Решение:
Давайте пройдёмся по элементам массива (for-of):
Для каждого элемента мы проверим, есть ли он в массиве с результатом.
Если есть, то игнорируем его, а если нет – добавляем к результатам.
*/
// function unique(arr) {
//     let unique = [];
//     for (const word of arr) {
//         if (unique.includes(word)) { 
//             continue
//         } else {
//             unique.push(word)
//         }
//     }
//     return unique;
//   }
  
//   let strings = ["кришна", "кришна", "харе", "харе",
//     "харе", "харе", "кришна", "кришна", ":-O"
//   ];
  
//   alert( unique(strings) ); // кришна, харе, :-
//   // моё решение работает, а в учебнике так:
//   function unique(arr) {
//     let result = [];
  
//     for (let str of arr) {
//       if (!result.includes(str)) {
//         result.push(str);
//       }
//     }
  
//     return result;
//   }

/*--------------=======================-----------------*/

/* Например, соберём все аргументы в массив args:

function sumAll(...args) { // args — имя массива
  let sum = 0;

  for (let arg of args) sum += arg;

  return sum;
}

alert( sumAll(1) ); // 1
alert( sumAll(1, 2) ); // 3
alert( sumAll(1, 2, 3) ); // 6

В примере ниже первые два аргумента функции станут именем и фамилией, а третий и последующие превратятся в массив titles:

function showName(firstName, lastName, ...titles) {
  alert( firstName + ' ' + lastName ); // Юлий Цезарь

  // Оставшиеся параметры пойдут в массив
  // titles = "Консул", "Император" 
  alert( titles0  ); // Консул
  alert( titles1  ); // Император
  alert( titles.length ); // 2
}

showName("Юлий", "Цезарь", "Консул", "Император"); */



/* Когда ...arr используется при вызове функции, он «расширяет» перебираемый объект arr в список аргументов.

Для Math.max:
let arr = 3, 5, 1 ;
alert( Math.max(...arr) ); // 5 (оператор "раскрывает" массив в список аргументов)

Этим же способом мы можем передать несколько итерируемых объектов:
let arr1 = 1, -2, 3, 4 ;
let arr2 = 8, 3, -8, 1 ;
alert( Math.max(...arr1, ...arr2) ); // 8

Мы даже можем комбинировать оператор расширения с обычными значениями:
let arr1 = 1, -2, 3, 4 ;
let arr2 = 8, 3, -8, 1 ;
alert( Math.max(1, ...arr1, 2, ...arr2, 25) ); // 25

Оператор расширения можно использовать и для слияния массивов:
let arr = 3, 5, 1 ;
let arr2 = 8, 9, 15 ;
let merged = 0, ...arr, 2, ...arr2 ;
alert(merged); // 0,3,5,1,2,8,9,15 (0, затем arr, затем 2, в конце arr2)

Например, оператор расширения подойдёт для того, чтобы превратить строку в массив символов:
let str = "Привет";
alert( ...str  ); // П,р,и,в,е,т




