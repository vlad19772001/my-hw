// ----=====---- FUNCTION ----=====----
"use strict";
// arguments когда не знаем сколько передадут в функцию аргументов, перебираем все подряд
// function datw() {
// 	let sum = 0;
// for (let i=0; i<arguments.length; i++) {
// 	sum += arguments[i];
// }
// return sum;
// }
// console.log(datw(1, 3, 4, 5, 23));


// Функция может вызвать саму себя. Например, вот функция рекурсивного вычисления факториала:
// function factorial(n) {
//   if ((n === 0) || (n === 1))
//     return 1;
//   else
//     return (n * factorial(n - 1));
// }

// возвращает true/false двумя способами
// function checkAge(age) {
// 	return (age > 18) ? true : confirm('Родители разрешили?');
//   }
// function checkAge(age) {
//   return (age > 18) || confirm('Родители разрешили?');
// }

// возвращает меньшее из двух чисел
// function min(a, b) {
// 	return (a<b) ? a: b;
// }
// console.log(min(2, 2));

// Напишите функцию pow(x,n), которая возвращает x в степени n.
// console.log(pow(3, 4)); //=81
// function pow (a, b) {
// 	let result = a;
// 	for (let i=1; i<b; i++) {
// 		 result = a*result; // result *= a
// 	}
// 	return result;
// }

// function showMessage(from, text) {
// 	console.log(from, text);
// }

// function isTrue(value) {
// 	return Boolean(value);
// }

// const name1 = prompt('Enter your name');
// const age = prompt('Your age');
// const resOfShowMessage = showMessage(name1, age);

// showMessage(name1, isTrue(5)); // -> from = 'Ann', text = 'hello'

// function checkUserAccess(age, status) {
// 	if (age >= 18) {
// 		return true;
// 	}
// 	if (status == 'admin') {
// 		return true;
// 	}
// 	return false;
// }

// const result = checkUserAccess(age, name1);
// console.log(result);

// function toStepen(number, stepen) {
// 	let result = 1;
// 	for (let i = 1; i <= stepen; i++) {
// 		result = result * number;
// 	}
// 	return result;
// }

// const res1 = toStepen(3, 4); //81

// const res2 = toStepen(2, 7); //128
// console.log(res1, res2);

// function sum(a, b) {
// 	return a + b;
// }

// //sum(1, 2);

// const min = function (a, b) {
// 	if (a < b) {
// 		return a;
// 	} else if (b < a) {
// 		return b;
// 	} else {
// 		return 'a=b';
// 	}
// };

// min(1, 2);

// // function min(a, b) {
// // 	if (a < b) {
// // 		return a;
// // 	} else if (b < a) {
// // 		return b;
// // 	} else {
// // 		return 'a=b';
// // 	}
// // }

// //min(3,5) //3
// //min(6,1) //1
// //min(2,2) //a=b

// let b = 10;

// function test2() {
// 	let b = 3;
// 	console.log('->', b); // 3
// }

// test2();

// console.log(b); // 10

// function test3() {
// 	b = 5;
// 	console.log('->', b); // 5
// }

// test3();

// console.log(b); // 5

// 1
// Функция счётчик (a,b)
// Два числа:
// первое число (с которог необходимо начинать счёт)
// второе (заканчивать счёт)
// первое число >= второго -> ошибка
// выводит в консоль числа от первого до второго

// counter(3,7) -> 3 4 5 6 7
// counter(3,3) -> Error
// counter(3,1) -> Error

// function easyCounter(num1, num2) {
// 	if (num1 >= num2) {
// 		console.log('Error');
// 		return null;
// 	}
// 	for (let i = num1; i <= num2; i++) {
// 		console.log(i);
// 	}
// }
// easyCounter(10, 10);
// easyCounter(15, 10);

// // написать функцию получает два числа если оба чётные возвращать их умноженияесли оба не чётные их суммуесли хоть одно разное возвращать не чётное оба числа проверить на коректность

// function twoNumbers(num1, num2) {
// 	if (typeof num1 !== 'number' || typeof num2 !== 'number') {
// 		return 'error';
// 	} else {
// 		if (num1 % 2 === 0 && num2 % 2 === 0) {
// 			return num1 * num2;
// 		} else if (num1 % 2 === 1 && num2 % 2 === 1) {
// 			return num1 + num2;
// 		} else {
// 			// if (num1 % 2 === 0) {
// 			// 	return num2;
// 			// } else if(num1 % 2 === 1) {
// 			// 	return num1;
// 			// }
// 			return num1 % 2 === 0 ? num2 : num1;
// 		}
// 	}
// }
// console.log(twoNumbers(5, 'a'));

// -------------- ARROW FUNCTIONS 

// function ask(question, yes, no) {
// 	if (confirm(question)) yes()
// 	else no();
//   }

//   ask(
// 	"Вы согласны?",
// 	function() { alert("Вы согласились."); },
// 	function() { alert("Вы отменили выполнение."); }
//   );
// // переделать в стрелочную
//   let newAsk = (question, yes, no) => {
// 	if (confirm(question)) yes()
// 	else no();
//   }
// ask(
// 	"Вы согласны?",
// 	()=>alert("Вы согласились."),
// 	()=>alert("Вы отменили выполнение."),
// )

// const sum = (a, b) => a*b;
// console.log(sum(5,7));
// let age = prompt("Сколько Вам лет?", 18);
// let welcome = (age < 18) ?
//   () => alert('Привет') :
//   () => alert("Здравствуйте!");
// welcome();

/**
 * Задание 1.
 *
 * Написать функцию-сумматор.
 *
 * Функция обладает двумя числовыми параметрами, и возвращает результат их сложения.
 *
 * Условия:
 * - Использовать функцию типа arrow function;
 * - Написать имплементация функции в двух синтаксических формах, которые позволяет стрелочная функция.
 */
// let sumator = (a, b) => a+b;

/**
 * Задание 2.
 * Написать функцию-сумматор всех своих параметров.
 * Функция принимает произвольное количество параметров.
 * Однако каждый из них обязательно должен быть числом.
 *
 * Генерировать ошибку, если:
 * - Если функция была вызвана менее, чем с двумя аргументами;
 * - Хоть один из аргументов не является допустимым числом (в ошибке указать его порядковый номер).
 *
 * Условия:
 * - Использовать тип функции arrow function;
 * - Использовать объект arguments запрещено.
 */
let sumator = (...parameters) => {  //... это масив с именем параметры
	if (parameters.length<2) {
		throw new Error('Функцию необходимо вызвать хотя-бы с двумя аргументами.');
	}
	let result = 0;
	for (let key of parameters) { // для каждого элемента массива
		if (typeof key !== "number" || Number.isNaN(key)) {
			throw new Error(`аргумент ${parameters.indexOf(key)+1} не является допустимым числом`)
		}
		result = result + key;
	}
	return result;
}
// console.log(sumator(3, 12, 6, 5, "max", 9))
console.log(sumator(3, 12, 6, 5, 9))

// const sumAll = (...parameters) => {
// 	if (parameters.length < 2) {
// 		throw new Error('Функцию необходимо вызвать хотя-бы с двумя аргументами.');
// 	}
//
// 	let accumulator = 0;
//
// 	for (const parameter of parameters) {
// 		if (typeof parameter !== 'number' || Number.isNaN(parameter)) {
// 			throw new Error(
// 				`Аргумент ${parameters.indexOf(parameter) +
// 				1} является не корректным числом.`,
// 			);
// 		}
//
// 		accumulator = accumulator + parameter;
// 	}
//
// 	return accumulator;
// };
//
// console.log(sumAll(1, 8, 16, 666, 1.3));
// console.log(sumAll(1, 2, 'string', 5));


/**
 * Задание 5.
 *
 * Написать калькулятор на функциях.
 *
 * Программа должна выполнять четыре математические операции:
 * - Сложение (add);
 * - Вычитание (subtract);
 * - Умножение (multiply);
 * - Деление (divide).
 *
 * Каждую математическую операцию должна выполнять отдельная функция.
 * Каждая такая функция обладает двумя параметрами-операндами,
 * и возвращает результат нужной операции над ними.
 *
 * Эти вспомогательный функции использует главная функция calculate,
 * которая обладает тремя параметрами:
 * - Первый — числовой тип, первый операнд;
 * - Второй — числовой тип, второй операнд;
 * - Третий — функцию, ссылка на ранее созданную вспомогательную функцию.
 *
 * Условия:
 * - Никаких проверок типов данных совершать не нужно;
 * - Обязательно использовать паттерн «callback».
 */

// const sum = (a, b) => a + b;
// const subtract = (a, b) => a - b;
// const multiply = (a, b) => a * b;
// const divide = (a, b) => a / b;
// const getMode = (a, b) => a % b;
//
// const calculate = (a, b, func) => {
// 	return func(a, b)
// }
//
// console.log(calculate(5, 9, sum));
// console.log(calculate(1, 4, subtract));
// console.log(calculate(3, 12, multiply));
// console.log(calculate(14, 7, divide));
// console.log(calculate(14, 7, getMode));




/**
 * Задание 4. Написать функцию-помощник кладовщика.
 *
 * Функция обладает одним параметром:
 * - Строка со списком товаров через запятую (water,banana,black,tea,apple).
 *
 * Функция возвращает строку в формате ключ-значение, где ключ — имя товара, а значение — его остаток на складе.
 * Каждый новый товар внутри строки должен содержатся на новой строке.
 *
 * Если какого-то товара на складе нет, в качестве остатка указать «not found».
 *
 * Условия:
 * - Имя товара не должны быть чувствительно к регистру;
 * - Дополнительных проверок совершать не нужно.
 */

/* Дано */
const store = {
	apple: 8,
	beef: 162,
	banana: 14,
	chocolate: 0,
	milk: 2,
	water: 16,
	coffee: 0,
	tea: 13,
	cheese: 0,
};

/* Решение

const getStoreBalance = list => {
	let resultString = '';
	let currentGood = '';
	let endIndex = 0;
	let startIndex = 0;
	list = list.toLowerCase();

	while (endIndex !== -1) {
		endIndex = list.indexOf(',', startIndex);
		if (endIndex === -1) {
			currentGood = list.slice(startIndex);
		} else {
			currentGood = list.slice(startIndex, endIndex);
		}
		let string = typeof store[currentGood] !== 'undefined' ? store[currentGood] : 'not found';
		resultString = resultString + `
      ${currentGood} - ${string}
    `
		startIndex = endIndex + 1;
	}
	return resultString;
}

console.log(getStoreBalance('Apple,Box,cheese,water,walter,milk'));
*/