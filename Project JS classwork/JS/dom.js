"use strict";
// document.addEventListener("DOMContentLoaded", ready);
// let analyticsData = { /* объект с собранными данными */ };
// window.addEventListener("unload", function() {
//   navigator.sendBeacon("/analytics", JSON.stringify(analyticsData));
// })

// /*
// "loading" – документ загружается.
// "interactive" – документ был полностью прочитан.
// "complete" – документ был полностью прочитан и все ресурсы (такие как изображения) были тоже загружены. */
// function work() { /*...*/ }
// if (document.readyState == 'loading') {
//   // ещё загружается, ждём события
//   document.addEventListener('DOMContentLoaded', work);
// } else {
//   // DOM готов!
//   work();
// }
// /** Типичный вывод:
// [1] начальный readyState:loading
// [2] readyState:interactive
// [2] DOMContentLoaded
// [3] iframe onload
// [4] img onload
// [4] readyState:complete
// [4] window onload */

// function checkPhoneKey(key) {
//   return (key >= '0' && key <= '9') || key == '+' || key == '(' || key == ')' || key == '-' ||
//     key == 'ArrowLeft' || key == 'ArrowRight' || key == 'Delete' || key == 'Backspace';
// }
// <input onkeydown="return checkPhoneKey(event.key)" placeholder="Введите телефон" type="tel"></input>

// // Вот небольшая функция для отображения текущей прокрутки:
// window.addEventListener('scroll', function() {
//   document.getElementById('showScroll').innerHTML = pageYOffset + 'px';
// });

// function sayHi(phraze, who) {
//   console.log(`${phraze}  ${who} !`);
// }
// setTimeout(sayHi, 1000, "Привет", "Джон") // вызовет один раз через 1с
// setInterval(sayHi, 2000, "Прииит", "Джоон") // будет периодически вызывать через 2с
// setTimeout("alert('Привет')", 2000);

// повторить с интервалом 2 секунды
// let timerId = setInterval(sayHi, 2000, "Прив,", "Джооон");
// clearInterval(timerId)
// остановить вывод через 5 секунд
// setTimeout(() => { clearInterval(timerId); alert('stop'); }, 8000);
// localStorage.setItem("Vlad", "kravch")
// let who = localStorage.getItem("Vlad")
// console.log(who);
// localStorage.clear()