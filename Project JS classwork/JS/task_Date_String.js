"use strict";

// Чтобы найти все вхождения подстроки, нужно запустить indexOf в цикле. Каждый раз, получив очередную позицию, начинаем новый поиск со следующей:

/*let str = 'Ослик Иа-Иа посмотрел на виадук';

let target = 'Иа'; // цель поиска

let pos = 0;
while (true) {
  let foundPos = str.indexOf(target, pos);
  if (foundPos == -1) break;

  alert( `Найдено тут: ${foundPos}` );
  pos = foundPos + 1; // продолжаем со следующей позиции
}

Тот же алгоритм можно записать и короче:

let str = "Ослик Иа-Иа посмотрел на виадук";
let target = "Иа";

let pos = -1;
while ((pos = str.indexOf(target, pos + 1)) != -1) {
  alert( pos );
}

str.lastIndexOf(substr, position)
Также есть похожий метод str.lastIndexOf(substr, position), который ищет с конца строки к её началу.

Он используется тогда, когда нужно получить самое последнее вхождение: перед концом строки или начинающееся до (включительно) определённой позиции.

*/




// function repeat(sourceString, times) {
//   if (sourceString === '') {
//       throw new Error('Error: First parameter should not be an empty string.')
//   }
//   if (typeof sourceString !== "string") {
//       throw new Error('Error: First parameter should be a string type.')
//   }
//   // if (typeof times !== "number") {
//   //     throw new Error('Error: Second parameter should be a number type.')
//   // }
//   if (isNaN(times)) {
//       throw new Error('Error: Second parameter should be a number type.')
//   }
//   let newString = '';
//   for (let i = 0; i < times; i++) {
//       newString = newString + sourceString;
//   }
//   return newString;
// }

// /* Пример */
// const string = 'Hello, world!';
// console.log(repeat(string, 3)); // Hello, world!Hello, world!Hello, world!
// console.log(repeat(string, 1)); // Hello, world!
// console.log(repeat(string, 2)); // Hello, world!Hello, world!
// console.log(repeat(string, 5)); // Hello, world!Hello, world!Hello, world!Hello, world!Hello, world!
// console.log(repeat(7, 5)); // Error: First parameter should be a string type.



// const capitalizeAndDoublify = string => {
//   let result = "";
//   for (let char of string){
//       result += char.repeat(2)
//   }
//   return result.toUpperCase()
// };

/* Пример */
// console.log(capitalizeAndDoublify('hello')); // HHEELLOO
// console.log(capitalizeAndDoublify('JavaScript!')); // JJAAVVAASSCCRRIIPPTT!!



// function truncate(string, desiredLength) {
//   if ((string.length + 3) > desiredLength) {
//       return string.slice(desiredLength - 3).concat('...')
//   }
// }

// /* Пример */
// console.log(truncate('Hello, world!', 12));



// const days = {
//   0: 'Воскресенье',
//   1: 'Понедельник',
//   2: 'Вторник',
//   3: 'Среда',
//   4: 'Thursday',
//   5: 'Friday',
//   6: 'Saturday',
// };

// /* Решение */
// const getDayAgo = (numberOfDays = 0) => {
//   const now = new Date();
//   const calculatedDate = now.getDate() - numberOfDays;
//   now.setDate(calculatedDate);

//   const finalWeekDay = now.getDay() /* получаем день недели в формате числа (0 - 6) */

//   return days[finalWeekDay]; /* Изымаем имя дня в формате строки из объекта. */
// };

// console.log(getDayAgo(9));



// =-=-+-+-=-=-+-+-=-=-+- DATE TIME +-=-=-+-+-=-=-+-+-=-=-+-+-
// let currentDate = new Date();
// alert( currentDate ); // показывает текущие дату и время
// let date = new Date(2016, 1, 28);
// date.setDate(date.getDate() + 2); // плюс 2 дня
// let date = new Date();
// date.setSeconds(date.getSeconds() + 70); // плюс 70 секунд

// let date = new Date(1977,5,14)
// alert(date.getDay())

// alert( date ); // 1 Mar 2016

// Выведите на экран текущую дату-время в формате '12:59:59 31.12.2014'.
// Для решения этой задачи напишите arrow-функцию,
// которая будет добавлять 0 перед днями и месяцами,
// которые состоят из одной цифры (из 1.9.2014 сделает 01.09.2014).
/* const returnDate = () => {

    let currentDate = new Date();
    let hours = currentDate.getHours();
    if (hours < 10) {
        hours = `0${hours}`
    }
    let minutes = currentDate.getMinutes();
    (minutes < 10) ? minutes = `0${minutes}` : "";
    let seconds = currentDate.getSeconds();
    (seconds < 10) ? seconds = `0${seconds}` : "";
    let year = currentDate.getFullYear();
    let month = currentDate.getMonth() + 1;
    (month < 10) ? month = `0${month}` : "";
    let date = currentDate.getDate();
    (date < 10) ? date = `0${date}` : "";
    let day = currentDate.getDay();

    switch (day) {
        case 0:
            day = "Воскресенье";
            break;
        case 1:
            day = "Понедельник";
            break;
        case 2:
            day = "Вторник";
            break;
        case 3:
            day = "Среда";
            break;
        case 4:
            day = "Четверг";
            break;
        case 5:
            day = "Пятница";
            break;
        case 6:
            day = "Суббота";
            break;
    }
    return alert(`Сейчас: ${day}  
Время: ${hours}:${minutes}:${seconds}   
Дата: ${date}.${month}.${year}`)
}

returnDate();
*/

/**
 * Задание 1.
 *
 * Написать имплементацию встроенной функции строки repeat(times).
 *
 * Функция должна обладать двумя параметрами:
 * - Целевая строка текст;
 * - Количество повторений целевой строки.
 *
 * Условия:
 * - Генерировать ошибку, если первый параметр не является строкой, а второй не является числом.
  */

// function repeat (sourceString, times) {
//     if (sourceString === "") {
//         throw new Error("String is empty")
//     }
//     if (typeof sourceString !== 'string') {
//         throw new Error("It is not text")
//     }
//     if (isNaN(times)) {
//         throw new Error("Second parameters is not a number type")
//     }
//     let newText = "";
//     for (let i=0; i<times; i++) {
//     newText = newText + sourceString;
//     }
//     return newText;
// }

/*
// Пример
const string = 'Ok, Google! ';
console.log(repeat(string, 3)); // Hello, world!Hello, world!Hello, world!
console.log(repeat(string, 1)); // Hello, world!
console.log(repeat(string, 2)); // Hello, world!Hello, world!
console.log(repeat(string, 5)); // Hello, world!Hello, world!Hello, world!Hello, world!Hello, world!
console.log(repeat(7, 2)); // Error: First parameter should be a string type.
*/


/**
 * Задание 2.
 * Написать функцию, capitalizeAndDoublify, которая переводит
 * символы строки в верхний регистр и дублирует каждый её символ.
 *
 * Условия:
 * - Использовать встроенную функцию repeat;
 * - Использовать встроенную функцию toUpperCase;
 * - Использовать цикл for...of.
 */

/* Решение
const capitalizeAndDoublify = text2 => {
let result = "";
for (let char of text2) {
    result+=char.repeat(2);
}
return result.toUpperCase()
};  */

/* Пример */
// console.log(capitalizeAndDoublify('hello')); // HHEELLOO
// console.log(capitalizeAndDoublify('JavaScript!')); // JJAAVVAASSCCRRIIPPTT!!

// let birdth = prompt("Enter your date of birth (dd.mm.yyyy)")
// let date = new Date('02.03.2012');
// let mydate  = new Date(birthday);
// console.log(birdth);
// let yearBorn = date.slice(7);
// console.log(date[0]);
// let monthBorn = date.date.slice(2, 4);
// console.log(yearBorn, monthBorn, dateBorn);

// function formatDate(dateString)
//     {
//         var allDate = date1.split(' ');
//         var thisDate = allDate[0].split('-');
//         var thisTime = allDate[1].split(':');
//         var newDate = [thisDate[2],thisDate[1],thisDate[0] ].join("-");
//         return newDate }

// alert(date2);
// let currentDate = new Date();
// alert( currentDate ); // показывает текущие дату и время
// let date = new Date(2016, 1, 28);
// date.setDate(date.getDate() + 2); // плюс 2 дня
// let date = new Date();
// date.setSeconds(date.getSeconds() + 70); // плюс 70 секунд

// let date = new Date(1977,5,14)
// alert(date.getDay())

// alert( date ); // 1 Mar 2016

// const createNewUser = (newUser) => {
//     let currentDate = new Date();
//     let selectFirstName = prompt("Enter your name");
//     let selectLastName = prompt("Enter your last name");
//     let selectBirthdayDate = prompt("Enter your birthday date (text in format dd.mm.yyyy)");
//     let convertBirthdayDate = new Date(selectBirthdayDate);
//     convertBirthdayDate.setDate(parseInt(selectBirthdayDate.substr(0, 2)));
//     convertBirthdayDate.setMonth(parseInt(selectBirthdayDate.substr(3,2)) - 1);
//     let currentYear;
//     let birthdayYear;
//     let user = {
//         firstName: selectFirstName,
//         lastName: selectLastName,
//         birthday: convertBirthdayDate,
//         getLogin(firstName, lastName) {
//             let firstCharOfName = user.firstName.substr(0, 1);
//             return firstCharOfName.toLowerCase() + user.lastName.toLowerCase();
//         },
// getAge(currentYear, birthdayYear) {
//      currentYear = currentDate.getFullYear();
//      birthdayYear = convertBirthdayDate.getFullYear();
//     console.log(currentYear - birthdayYear);
//     return currentYear - birthdayYear;
// }
//     }
//     Object.defineProperty(user, 'firstName', {
//         writable: false,
//         value: selectFirstName
//     });
//     Object.defineProperty(user, 'lastName', {
//         writable: false,
//         value: selectLastName
//     });
//     console.log(user.getLogin(), user.getAge());
//     return newUser;
// }
// createNewUser();

// let birthday = prompt("Enter your birthday date (text in format dd.mm.yyyy)");
let birthday = "14.06.1977";
// let birthday1 = birthday.split('.')
// let birthday2 = birthday1.reverse().join("-")
// let date = new Date(birthday2)
// alert(date)

let currentDate = new Date();
// let raznica = currentDate-date
// console.log(parseInt( raznica/1000/3600/24/365.25));

// let currentBirthday = new Date(birthday.split(".").reverse().join("-"))
// let age = parseInt((currentDate - currentBirthday)/1000/3600/24/365.25)

let currentBirthday = new Date(birthday.split(".").reverse().join("-"));
      const age = parseInt((currentDate - currentBirthday)/1000/3600/24/365.25);
    //   return age; 
let year = currentDate.getFullYear()
console.log(age, year);