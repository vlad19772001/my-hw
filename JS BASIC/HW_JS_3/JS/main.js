"use strict";


// Теория. 
// функцию создают тогда, когда нужно повторять одно и то-же вычисление в нескольких местах программы. 
// поэтому что-бы не писать один и тот-же код в разных местах, объявляют функцию, в теле которой прописывают исполняемый код, а потом в разных местах просто вызывают имя функции и передают ей аргументы, котлорые нужны для вычисления функции. 

let num1, num2 = "";
let oper;
do {
    num1 = +prompt("Please, enter number 1 ") 
} while (!Number.isInteger(num1));
do {
    num2 = +prompt("Please, enter number 2 ") 
} while (!Number.isInteger(num2));

do {
  oper = prompt("Please, enter operation ") 
} while (oper === "");

function calcOper(a, b, oper) {
  if (oper==="/") {return a / b};
  if (oper==="*") {return a * b};
  if (oper==="-") {return a - b};
  if (oper==="+") {return a + b};  
}
console.log(`Result: ${calcOper(num1, num2, oper)}`);

// Update. Переделал через switch-case, как указано в дополнит. пояснениях

function withSwith(a, b, oper) {
  switch (oper) {
    case "/":
      return a/b;
      case "*":
        return  a*b;
        case "-":
          return a-b;
          case "+":
            return a+b;
    default:
      break;
  }
}
console.log(`Result with swith-case: ${withSwith(num1, num2, oper)}`);
