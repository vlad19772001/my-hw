"use strict";
const theme = document.querySelector(".theme__button");
const style = document.querySelector("#style");
const normalTheme = "./css/style.css";
const darkTheme = "./css/style_dark.css";

window.addEventListener("load", () => {
    let colorTheme = localStorage.getItem("colorTheme");
    if (colorTheme) {
        style.setAttribute("href", colorTheme);
    }
});

theme.addEventListener("click", () => {
    if (style.getAttribute("href") === normalTheme) {
        style.setAttribute("href", darkTheme);
        localStorage.setItem("colorTheme", darkTheme);
    } else {
        style.setAttribute("href", normalTheme);
        localStorage.setItem("colorTheme", normalTheme);
    }
});