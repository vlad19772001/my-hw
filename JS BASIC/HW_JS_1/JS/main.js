"use strict";
// Теория. const используем когда нужно задать неизменное значение для переменной. 
// Например, год рождения или код цвета. 
// переменная let назначается один раз, видна только ниже её объявления, её значение можно изменять, 
// и если мы её назначаем в корне, то она видна везде в корне и в блоках.
// А вот если {внутри фиг.скобок}, то и видна она только внутри, поэтому имя переменной
// может повторяться и это не вызовет конфликта.
// var устарешвший, который как и let, но его видно и в скобках и за скобками, и даже если объявим внутри 
// какого-то блока или функции, то и внутри другого блока его тоже будет видно и он будет учитываться.
// а не пользоваться varом, скорее всего, советуют из-за того, что нужно постоянно помнить все именна таких переменных
// и не задублировать их в разных местах кода. 

let userName="";
let userAge="";

do {
    userName = prompt("Enter your name");
} while (userName === "");

do {
    userAge = +prompt ("Enter your age");
} while (!userAge || userAge<0 || Number.isNaN(userAge)); // 

if (userAge < 18) { alert ("You are not allowed to visit this website");} 
    else {
    if (userAge >= 18 && userAge <= 22 ) { 
        if (confirm ("Are you sure you want to continue?")) {
            alert (`Welcome, ${userName}`);} 
            else  {alert ("You are not allowed to visit this website");}
}   else {
        if (userAge > 22 ) { alert (`Welcome, ${userName}`);
    } else {
        alert("Ты ввел неверные данные");
    }}}

