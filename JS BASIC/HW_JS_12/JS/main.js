"use strict";

const wrapper = document.querySelector(".images-wrapper");
const images = document.querySelectorAll(".image-to-show");
const start = document.createElement("button");
const stop = document.createElement("button");
start.innerText = "start";
stop.innerText = "stop";
wrapper.before(start, stop);

start.addEventListener("click", showImage);

function showImage() {
    start.setAttribute("disabled", true);
    stop.removeAttribute("disabled", true);

    let count = 0;
    let timer = setInterval(() => {
        if (count >= images.length) {
            count = 0;
        }

        images.forEach((item) => {
            item.classList.add("none");
        });

        images[count].classList.remove("none");

        count++;
    }, 1200);
    stop.addEventListener("click", () => {
        start.removeAttribute("disabled", true);
        stop.setAttribute("disabled", true);
        clearTimeout(timer);
    });
}