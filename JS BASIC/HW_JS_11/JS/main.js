"use strict";

document.body.addEventListener("keyup", (event) => {
    const keys = document.querySelectorAll(".btn");

    let myKey = event.key.toLowerCase();
    keys.forEach((element) => {
        element.classList.remove("active");
        let letter = element.innerText.toLowerCase();
        if (myKey === letter) {
            element.classList.add("active");
        }
    });
});