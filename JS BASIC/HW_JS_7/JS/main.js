"use strict";
// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)

/* HW #7. Show list on page.
1). Когда изучаете материалы, разные методы / функции, всегда обращайте внимание на примеры. Например, важно понимать, что метод map -это не тоже самое, что и метод forEach. Метод map не предусмотрен для того, чтобы просто  перебирать  массив  и  в  процессе  перебора  выполнять  какие-то команды с элементами массива. Этот метод используется для трансформации массива.  Например,  из  массива  [1,  2,  3]  можно  получить  [‘<li>1</li>’, ‘<li>2</li>’, ‘<li>3</li>’].2). Можно ли использовать метод join, чтобы из массива строк получить HTML-строку? Если продолжать мысль выше. )
3).  Шаблонная  строка -это  то,  что  внутри  косых  кавычек:  ``Интерполяция -это когда вы внутри строки разные выражения вставляете: ${...} Внутри интерполяции можно писать любые выражения, в том числе и вызывать методы map и join через цепочку. Допустим `<ul>${..........}</ul>`
4). Допустим, вы получили финальную HTML-строку, которую хотите вставить на страницу. Как это сделать? Используйте  Element.insertAdjacentHTML()5). Для опционального задания нужно использовать рекурсию, если решите его выполнить. 
*/
const arrayOne = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arrayTwo = ["1", "2", "3", "sea", "user", 23];

// variant 1

const convert = function(array, place = document.body) {
    let taskList = document.createElement("ul");
    let newArray = array.map(function(item) {
        let task = document.createElement("li");
        task.innerHTML = item;
        taskList.append(task);
    });
    place.append(taskList);
};
convert(arrayOne, document.body.main);
convert(arrayTwo);

// variant 2
const convert2 = function(array, place = document.body) {
    let newArray = array.map(function(item) {
        return `<li>${item}</li>`;
    });
    place.insertAdjacentHTML("afterbegin", newArray.join(""));
};

convert2(arrayOne, document.body.main);
convert2(arrayTwo);