"use strict";

const inputPrice = document.querySelector(".price");
const message = document.createElement("div");
const priceBlock = document.createElement("div");

inputPrice.addEventListener("focus", (event) => {
    if (message) {
        message.remove();
        inputPrice.value = null;
    }
    inputPrice.className = "price__green";
});
inputPrice.addEventListener("blur", () => {
    let price = +inputPrice.value;
    if (!price || price <= 0 || price === NaN) {
        inputPrice.className = "price__red";
        message.innerText = "Enter correct price";
        inputPrice.after(message);
    } else {
        inputPrice.className = "price";
        const result = document.createElement("span");
        const cancel = document.createElement("button");
        result.innerText = `Текущая цена: ${price}`;
        cancel.innerText = "X";
        const div = document.createElement("div");
        div.classList.add("inline-block");
        div.append(result, cancel);
        priceBlock.append(div);
        inputPrice.before(priceBlock);
        inputPrice.value = null;
    }
});
priceBlock.addEventListener("click", function(event) {
    if (event.target.closest("button")) {
        event.target.parentElement.remove();
    } else {}
});

/**
 *     
 * 
 *     const result = document.createElement("span");
        const cancel = document.createElement("button");
        result.innerText = price;
        cancel.innerText = "X";
        const div = document.createElement("div");
        // priceBlock.setAttribute("display", "block");
        priceBlock.classList.add("priceBlock");
        div.classList.add("priceDiv");
        div.append(result, cancel);
        priceBlock.append(div);
        inputPrice.before(priceBlock);
        inputPrice.value = null;
    // inputPrice.style.borderColor = "chartreuse";
    // price.style.outline = "auto";
            // div.setAttribute("display", "inline-block");
        // priceBlock.setAttribute("display", "block");
        // priceBlock.classList.add("priceBlock");
            // inputPrice.style.borderColor = "initial";
        inputPrice.style.borderColor = "red";

        // event.target.remove();
            inputPrice.style.outline = "none";
    inputPrice.style.outline = "none";
        inputPrice.classList.remove("price__green");




 */